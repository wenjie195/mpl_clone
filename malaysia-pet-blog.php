<?php
if (session_id() == "")
{
    session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// $articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC LIMIT 50 ");
$articles = getArticles($conn, " WHERE display = 'Yes' ORDER BY date_created DESC");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
    
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Daily Tips" />
<title>Daily Tips</title>
<meta property="og:description" content="Daily Tips" />
<meta name="description" content="Daily Tips" />
<meta name="keywords" content="Daily Tips">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php echo '<script type="text/javascript" src="js/jquery-3.3.1.min.js"></script>'; ?>

<?php include 'header.php'; ?>
 

<div class="width100 same-padding overflow min-height menu-distance2">
<h1 class="green-text user-title left-align-title">Daily Tips</h1>

    <div class="clear"></div>
    <div class="blog-padding-big-div">
    <?php
    $conn = connDB();
    if($articles)
    {
        for($cnt = 0;$cnt < count($articles) ;$cnt++)
        {
        ?>
        
        <a href='pet-blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>' class="opacity-hover">
            <div class="two-div-width shadow-white-box blog-box opacity-hover">
            	<a href='pet-blog.php?id=<?php echo $articles[$cnt]->getArticleLink();?>' class="block-a malay-pet-a"></a>
                <div class="left-img-div2">
                	<a href="uploadsArticle/<?php echo $articles[$cnt]->getTitleCover();?>" class="progressive replace">
                                <img src="img/cover-tiny.jpg" class="preview width100" alt="<?php echo $articles[$cnt]->getTitle();?>" title="<?php echo $articles[$cnt]->getTitle();?>" />
                    </a>
                   
                </div>

                <div class="right-content-div3">
                    <h3 class="article-title text-overflow">
                        <?php echo $articles[$cnt]->getTitle();?>
                    </h3>
                    <p class="date-p">
                        <?php echo $date = date("d-m-Y",strtotime($articles[$cnt]->getDateCreated()));?>
                    </p>
                    <p class="right-content-p">
                        <?php echo $description = $articles[$cnt]->getKeywordOne();?>
                    </p>
                </div>
            </div>
        </a>

        <?php
        }
        ?>
    <?php
    }
    $conn->close();
    ?>
    </div>
</div>


<style type="text/css">

.blog-a .hover1a{
    display:none !important;}
.blog-a .hover1b{
    display:inline-block !important;}						
</style>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Article Reported !"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Unable to make a report for the article !!";
        }
        else if($_GET['type'] == 3)
        {
            $messageType = "ERROR !!";
        }
        else if($_GET['type'] == 4)
        {
            $messageType = "ERROR <br> Unable to store in reported article !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>