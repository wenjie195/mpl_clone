<?php
if (session_id() == "")
{
     session_start();
}
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/../classes/Orders.php';
require_once dirname(__FILE__) . '/../classes/ProductOrders.php';

require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';
require_once dirname(__FILE__) . '/allNoticeModals.php';

    if($_SERVER['REQUEST_METHOD'] == 'POST')
    {
        $conn = connDB();

        $uid = $_SESSION['uid'];
        //$id = $asd;
        // $id = $_POST["order_id"];
        $id = rewrite($_POST["order_id"]);
        $username = rewrite($_POST["insert_username"]);

        $bankName = rewrite($_POST["insert_bankname"]);
        $bankAccountHolder = rewrite($_POST["insert_bankaccountholder"]);
        $bankAccountNo = rewrite($_POST["insert_bankaccountnumber"]);

        $names = rewrite($_POST["insert_name"]);
        $contactNo = rewrite($_POST["insert_contactNo"]);
        // $email = rewrite($_POST["insert_email"]);
        $address_1 = rewrite($_POST["insert_address_1"]);
        $address_2 = rewrite($_POST["insert_address_2"]);
        $city = rewrite($_POST["insert_city"]);
        $zipcode = rewrite($_POST["insert_zipcode"]);
        // $postcode = rewrite($_POST["insert_postcode"]);
        $state = rewrite($_POST["insert_state"]);
        $country = rewrite($_POST["insert_country"]);

        $subtotal = rewrite($_POST["insert_subtotal"]);
        $total = rewrite($_POST["insert_total"]);
        // $payment_status = rewrite($_POST["insert_paymentstatus"]);

        $payment_method = rewrite($_POST["payment_method"]);
        $payment_amount = rewrite($_POST["payment_amount"]);
        $payment_bankreference = rewrite($_POST["payment_bankreference"]);
        $payment_date = rewrite($_POST["payment_date"]);
        $payment_time = rewrite($_POST["payment_time"]);
        $payment_status = rewrite($_POST["payment_status"]);

        $receiptName = $_FILES['file']['name'];
        $target_dir = "../receipt/";
        $target_file = $target_dir . basename($_FILES["file"]["name"]);

        $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));

        // Valid file extensions
        $extensions_arr = array("jpg","jpeg","png","gif");

        if( in_array($imageFileType,$extensions_arr) ){


         move_uploaded_file($_FILES['file']['tmp_name'],$target_dir.$name);
       }

        // $user = getUser($conn," username = ?   ",array("username"),array($username),"s");
        // $orderUid = getOrders($conn," uid = ?   ",array("uid"),array($uid),"s");
        $orderUid = getOrders($conn," id = ? ",array("id"),array($id),"i");

        if(!$orderUid)
        {
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";
            //echo "save to database";
            if($username)
            {
                array_push($tableName,"username");
                array_push($tableValue,$username);
                $stringType .=  "s";
            }

            if($bankName)
            {
                array_push($tableName,"bank_name");
                array_push($tableValue,$bankName);
                $stringType .=  "s";
            }
            if($bankAccountHolder)
            {
                array_push($tableName,"bank_account_holder");
                array_push($tableValue,$bankAccountHolder);
                $stringType .=  "s";
            }
            if($bankAccountNo)
            {
                array_push($tableName,"bank_account_no");
                array_push($tableValue,$bankAccountNo);
                $stringType .=  "s";
            }

            if($names)
            {
                array_push($tableName,"name");
                array_push($tableValue,$names);
                $stringType .=  "s";
            }
            if($contactNo)
            {
                array_push($tableName,"contactNo");
                array_push($tableValue,$contactNo);
                $stringType .=  "s";
            }
            // if($email)
            // {
            //     array_push($tableName,"email");
            //     array_push($tableValue,$email);
            //     $stringType .=  "s";
            // }
            if($address_1)
            {
                array_push($tableName,"address_line_1");
                array_push($tableValue,$address_1);
                $stringType .=  "s";
            }
            if($address_2)
            {
                array_push($tableName,"address_line_2");
                array_push($tableValue,$address_2);
                $stringType .=  "s";
            }
            if($city)
            {
                array_push($tableName,"city");
                array_push($tableValue,$city);
                $stringType .=  "s";
            }
            if($zipcode)
            {
                array_push($tableName,"zipcode");
                array_push($tableValue,$zipcode);
                $stringType .=  "s";
            }
            if($state)
            {
                array_push($tableName,"state");
                array_push($tableValue,$state);
                $stringType .=  "s";
            }
            if($country)
            {
                array_push($tableName,"country");
                array_push($tableValue,$country);
                $stringType .=  "s";
            }

            if($subtotal)
            {
                array_push($tableName,"subtotal");
                array_push($tableValue,$subtotal);
                $stringType .=  "d";
            }
            if($total)
            {
                array_push($tableName,"total");
                array_push($tableValue,$total);
                $stringType .=  "d";
            }
            // if($payment_status)
            // {
            //     array_push($tableName,"payment_status");
            //     array_push($tableValue,$payment_status);
            //     $stringType .=  "s";
            // }

            if($payment_method)
            {
                array_push($tableName,"payment_method");
                array_push($tableValue,$payment_method);
                $stringType .=  "s";
            }
            if($payment_amount)
            {
                array_push($tableName,"payment_amount");
                array_push($tableValue,$payment_amount);
                $stringType .=  "i";
            }
            if($payment_bankreference)
            {
                array_push($tableName,"payment_bankreference");
                array_push($tableValue,$payment_bankreference);
                $stringType .=  "s";
            }
            if($payment_date)
            {
                array_push($tableName,"payment_date");
                array_push($tableValue,$payment_date);
                $stringType .=  "s";
            }
            if($payment_time)
            {
                array_push($tableName,"payment_time");
                array_push($tableValue,$payment_time);
                $stringType .=  "s";
            }
            if($payment_status)
            {
                array_push($tableName,"payment_status");
                array_push($tableValue,$payment_status);
                $stringType .=  "s";
            }
            if($receiptName)
            {

              move_uploaded_file($_FILES['file']['tmp_name'],$target_file);

                array_push($tableName,"receipt");
                array_push($tableValue,$receiptName);
                $stringType .=  "s";
            //}
            }

            array_push($tableValue,$id);
            $stringType .=  "s";
            $orderUpdated = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
            if($orderUpdated)
            {
                // echo "success";
                // echo "<br>";
                // echo $id."<br>";
                // echo $username."<br>";
                // echo $bankName."<br>";
                // echo $bankAccountHolder."<br>";
                // echo $bankAccountNo."<br>";
                // echo $name."<br>";
                // echo $contactNo."<br>";
                // echo $address_1."<br>";
                // echo $address_2."<br>";
                // echo $city."<br>";
                // echo $zipcode."<br>";
                // echo $state."<br>";
                // echo $country."<br>";
                // echo $subtotal."<br>";
                // echo $total."<br>";
                // echo $payment_method."<br>";
                // echo $payment_bankreference."<br>";
                // echo $payment_date."<br>";
                // echo $payment_time."<br>";
                // echo $payment_status."<br>";

                // header('Location: ../shipping.php');
                  $_SESSION['messageType'] = 1;
                header('Location: ../purchaseHistory.php?type=1');
            }
            else
            {
                //echo "fail";
                $_SESSION['messageType'] = 1;
                header('Location: ../checkout.php?type=1');
            }
        }
        else
        {
            //echo "dunno";
            $_SESSION['messageType'] = 1;
            header('Location: ../checkout.php?type=2');
        }

    }
else
{
    header('Location: ../product.php');
    // header('Location: ../index.php');
}
?>
