<?php
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/CreditCard.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $sliderUid = rewrite($_POST["slider_uid"]);
    $status = "Delete";

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $sliderUid."<br>";
    // echo $status."<br>";

    if(isset($_POST['slider_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }    

        array_push($tableValue,$sliderUid);
        $stringType .=  "s";
        $deleteSlider = updateDynamicData($conn,"slider"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($deleteSlider)
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../slider.php?type=3');
        }
        else
        {
            $_SESSION['messageType'] = 1;
            header('Location: ../slider.php?type=4');
        }
    }
    else
    {
        $_SESSION['messageType'] = 1;
        header('Location: ../slider.php?type=5');
    }
    
}
else
{
     header('Location: ../index.php');
}
?>
