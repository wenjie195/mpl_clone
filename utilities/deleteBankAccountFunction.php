<?php
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Bank.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $bankNumber = rewrite($_POST["bank_acc_number"]);
    $bankUser = rewrite($_POST["bank_user"]);
    $status = "Delete";

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $bankNumber."<br>";
    // echo $bankUser."<br>";

    if(isset($_POST['bank_acc_number']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($status)
        {
            array_push($tableName,"status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }    

        // array_push($tableValue,$cardNumber);
        // $stringType .=  "s";
        // $deleteCreditCard = updateDynamicData($conn,"credit_card"," WHERE card_no = ? ",$tableName,$tableValue,$stringType);

        array_push($tableValue,$bankNumber,$bankUser);
        $stringType .=  "ss";
        $deleteCreditCard = updateDynamicData($conn,"bank"," WHERE bank_account_no = ? AND username = ? ",$tableName,$tableValue,$stringType);
        if($deleteCreditCard)
        {
            // echo "credit card deleted";
            $_SESSION['messageType'] = 1;
            header('Location: ../editBankDetails.php?type=9');
        }
        else
        {
            // echo "fail";
            $_SESSION['messageType'] = 1;
            header('Location: ../editBankDetails.php?type=10');
        }
    }
    else
    {
        // echo "error";
        $_SESSION['messageType'] = 1;
        header('Location: ../editBankDetails.php?type=6');
    }
    
}
else
{
     header('Location: ../index.php');
}
?>
