<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../adminAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Seller.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$timestamp = time();

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST['uid']);

    // $name = rewrite($_POST['update_name']);
    // $slug = rewrite($_POST['update_slug']);

    $stringOne = ($_POST['update_name']);
    $name = preg_replace('/[^\p{L}\p{N}\s][^(\x20-\x7F)]*/u', '', $stringOne);
    $updatedCompName = str_replace(' ', '-', trim($name));
    $slug = $updatedCompName;

    $reg = rewrite($_POST['update_reg']);

    // $contact = rewrite($_POST['update_contact']);

    $sellerContact= rewrite($_POST['update_contact']);
    $link = "https://api.whatsapp.com/send?phone=6";
    $contact = $link.$sellerContact;

    $address = rewrite($_POST['update_address']);
    $state = rewrite($_POST['update_state']);
    $status = rewrite($_POST['update_status']);
    $contactPerson = rewrite($_POST['update_contact_person']);
    $contactPersonNo = rewrite($_POST['update_contact_personNo']);
    $experience = rewrite($_POST['update_experience']);
    $cert = rewrite($_POST['update_cert']);
    // $services = rewrite($_POST['update_servives']);
    $breed = rewrite($_POST['update_breed']);
    $info = rewrite($_POST['update_info']);

    $infoTwo = rewrite($_POST['update_info_two']);
    $infoThree = rewrite($_POST['update_info_three']);
    $infoFour = rewrite($_POST['update_info_four']);

    $email = rewrite($_POST['update_email']);

    $newFavorite = $_POST["petri"];
    $newFavoriteImp = implode(",",$newFavorite);


    $seller = getSeller($conn," uid = ? ",array("uid"),array($uid),"s");    
    if(!$seller)
    // if(isset($_POST['editSubmit']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($name)
        {
            array_push($tableName,"company_name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
        if($slug)
        {
            array_push($tableName,"slug");
            array_push($tableValue,$slug);
            $stringType .=  "s";
        }
        if($reg)
        {
            array_push($tableName,"registration_no");
            array_push($tableValue,$reg);
            $stringType .=  "s";
        }
        if($contact)
        {
            array_push($tableName,"contact_no");
            array_push($tableValue,$contact);
            $stringType .=  "s";
        }
        if($address)
        {
            array_push($tableName,"address");
            array_push($tableValue,$address);
            $stringType .=  "s";
        }
        if($state)
        {
            array_push($tableName,"state");
            array_push($tableValue,$state);
            $stringType .=  "s";
        }
        if($status)
        {
            array_push($tableName,"account_status");
            array_push($tableValue,$status);
            $stringType .=  "s";
        }
        if($contactPerson)
        {
            array_push($tableName,"contact_person");
            array_push($tableValue,$contactPerson);
            $stringType .=  "s";
        }
        if($contactPersonNo)
        {
            array_push($tableName,"contact_person_no");
            array_push($tableValue,$contactPersonNo);
            $stringType .=  "s";
        }
        if($experience)
        {
            array_push($tableName,"experience");
            array_push($tableValue,$experience);
            $stringType .=  "s";
        }
        if($cert)
        {
            array_push($tableName,"cert");
            array_push($tableValue,$cert);
            $stringType .=  "s";
        }
        if($breed)
        {
            array_push($tableName,"breed_type");
            array_push($tableValue,$breed);
            $stringType .=  "s";
        }
        if($info)
        {
            array_push($tableName,"other_info");
            array_push($tableValue,$info);
            $stringType .=  "s";
        }
        if($infoTwo)
        {
            array_push($tableName,"info_two");
            array_push($tableValue,$infoTwo);
            $stringType .=  "s";
        }
        if($infoThree)
        {
            array_push($tableName,"info_three");
            array_push($tableValue,$infoThree);
            $stringType .=  "s";
        }
        if($infoFour)
        {
            array_push($tableName,"info_four");
            array_push($tableValue,$infoFour);
            $stringType .=  "s";
        }

        // if($imageOne)
        // {
        //     array_push($tableName,"company_logo");
        //     array_push($tableValue,$imageOne);
        //     $stringType .=  "s";
        // }
        // if($imageTwo)
        // {
        //     array_push($tableName,"company_pic");
        //     array_push($tableValue,$imageTwo);
        //     $stringType .=  "s";
        // }

        if($newFavoriteImp)
        {
            array_push($tableName,"services");
            array_push($tableValue,$newFavoriteImp);
            $stringType .=  "s";
        }
        if($email)
        {
            array_push($tableName,"email");
            array_push($tableValue,$email);
            $stringType .=  "s";
        }
        array_push($tableValue,$uid);
        $stringType .=  "s";
        $updateSellerDetails = updateDynamicData($conn,"seller"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($updateSellerDetails)
        {
            // $_SESSION['messageType'] = 1;
            // echo "<script>alert('Data Updated and Stored !');window.location='../seller.php'</script>"; 


            if(isset($_POST['editSubmit']))
            {   
                $tableName = array();
                $tableValue =  array();
                $stringType =  "";
                //echo "save to database";
                if($name)
                {
                    array_push($tableName,"name");
                    array_push($tableValue,$name);
                    $stringType .=  "s";
                }
                if($contact)
                {
                    array_push($tableName,"phone_no");
                    array_push($tableValue,$contact);
                    $stringType .=  "s";
                }
                if($email)
                {
                    array_push($tableName,"email");
                    array_push($tableValue,$email);
                    $stringType .=  "s";
                }
                array_push($tableValue,$uid);
                $stringType .=  "s";
                $updateUserDetails = updateDynamicData($conn,"user"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
                if($updateUserDetails)
                {
                    // echo "<script>alert('Data Updated and Stored !');window.location='../seller.php'</script>"; 
                    $_SESSION['messageType'] = 2;
                    header('Location: ../seller.php?type=1');
                }
                else
                {
                    // echo "<script>alert('Fail to Update Data !');window.location='../seller.php'</script>"; 
                    $_SESSION['messageType'] = 2;
                    header('Location: ../seller.php?type=2');
                }
            }
            else
            {
                // echo "<script>alert('ERROR on User table !!');window.location='../seller.php'</script>"; 
                $_SESSION['messageType'] = 2;
                header('Location: ../seller.php?type=3');
            }


        }
        else
        {
            // echo "<script>alert('Fail to Update Data on Seller table !!');window.location='../seller.php'</script>"; 
            $_SESSION['messageType'] = 2;
            header('Location: ../seller.php?type=4');
        }
    }
    else
    {
        // echo "<script>alert('ERROR on Seller table !!');window.location='../seller.php'</script>"; 
        $_SESSION['messageType'] = 2;
        header('Location: ../seller.php?type=3');
    }

}
else
{
    header('Location: ../index.php');
}
?>