<?php
require_once dirname(__FILE__) . '/../sellerAccess.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/../classes/Article.php';
require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';


if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $articleUid = rewrite($_POST["article_uid"]);
    $display = "Delete";

    // //   FOR DEBUGGING
    // echo "<br>";
    // echo $bankNumber."<br>";
    // echo $bankUser."<br>";

    if(isset($_POST['article_uid']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
        //echo "save to database";
        if($display)
        {
            array_push($tableName,"display");
            array_push($tableValue,$display);
            $stringType .=  "s";
        }    

        array_push($tableValue,$articleUid);
        $stringType .=  "s";
        $deleteArticle = updateDynamicData($conn,"articles"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
        if($deleteArticle)
        {
            // echo "credit card deleted";
            $_SESSION['messageType'] = 1;
            header('Location: ../sellerPendingArticle.php?type=4');
        }
        else
        {
            // echo "fail";
            $_SESSION['messageType'] = 1;
            header('Location: ../sellerPendingArticle.php?type=5');
        }
    }
    else
    {
        // echo "error";
        $_SESSION['messageType'] = 1;
        header('Location: ../sellerPendingArticle.php?type=3');
    }
    
}
else
{
     header('Location: ../index.php');
}
?>
