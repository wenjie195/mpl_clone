<?php
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/product.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
    $conn = connDB();

    $uid = rewrite($_POST['uid']);
    $category= rewrite($_POST['update_category']);
    $brand = rewrite($_POST['update_brand']);
    $name = rewrite($_POST['update_name']);
    $sku = rewrite($_POST['update_sku']);
    $slug = rewrite($_POST['update_slug']);
    $type = rewrite($_POST['update_animal_type']);
    $expiry = rewrite($_POST['update_expiry_date']);
    $status = rewrite($_POST['update_status']);
    $description = rewrite($_POST['update_description']);
    $keyword = rewrite($_POST['update_keyword_one']);

    $link = rewrite($_POST['update_link']);
    if($link != '')
    {
        $tubeOne = "Yes";
    }
    else
    {
        $tubeOne = "No";
    }


    //   FOR DEBUGGING 
    //  echo "<br>";
    //  echo $category."<br>";
    //  echo $brand ."<br>";
    //  echo $name."<br>";
    //  echo $sku."<br>";
    //  echo $slug."<br>";
    //  echo $type."<br>";
    //  echo $expiry."<br>";
    //  echo $status."<br>";
    //  echo $description."<br>";
    //  echo $link."<br>";
    //  echo $keyword."<br>";
}

if(isset($_POST['editSubmit']))
{   
    $tableName = array();
    $tableValue =  array();
    $stringType =  "";

    //echo "save to database";
    if($category)
    {
        array_push($tableName,"category");
        array_push($tableValue,$category);
        $stringType .=  "s";
    }

    if($brand)
    {
        array_push($tableName,"brand");
        array_push($tableValue,$brand);
        $stringType .=  "s";
    }

    if($name)
    {
        array_push($tableName,"name");
        array_push($tableValue,$name);
        $stringType .=  "s";
    }

    if($sku)
    {
        array_push($tableName,"sku");
        array_push($tableValue,$sku);
        $stringType .=  "s";
    }

    if($slug)
    {
        array_push($tableName,"slug");
        array_push($tableValue,$slug);
        $stringType .=  "s";
    }

    if($type)
    {
        array_push($tableName,"animal_type");
        array_push($tableValue,$type);
        $stringType .=  "s";
    }

    if($expiry)
    {
        array_push($tableName,"expiry_date");
        array_push($tableValue,$expiry);
        $stringType .=  "s";
    }

    if($status)
    {
        array_push($tableName,"status");
        array_push($tableValue,$status);
        $stringType .=  "s";
    }

    if($description)
    {
        array_push($tableName,"description");
        array_push($tableValue,$description);
        $stringType .=  "s";
    }

    if($link)
    {
        array_push($tableName,"link");
        array_push($tableValue,$link);
        $stringType .=  "s";
    }

    if($keyword)
    {
        array_push($tableName,"keyword_one");
        array_push($tableValue,$keyword);
        $stringType .=  "s";
    }

    array_push($tableValue,$uid);
    $stringType .=  "s";
    $updateProductDetails = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
    if($updateProductDetails)
    {

        if(isset($_POST['editSubmit']))
        {   
            $tableName = array();
            $tableValue =  array();
            $stringType =  "";

            //echo "save to database";
            if($category)
            {
                array_push($tableName,"category");
                array_push($tableValue,$category);
                $stringType .=  "s";
            }

            if($brand)
            {
                array_push($tableName,"brand");
                array_push($tableValue,$brand);
                $stringType .=  "s";
            }

            if($name)
            {
                array_push($tableName,"name");
                array_push($tableValue,$name);
                $stringType .=  "s";
            }

            if($sku)
            {
                array_push($tableName,"sku");
                array_push($tableValue,$sku);
                $stringType .=  "s";
            }

            if($slug)
            {
                array_push($tableName,"slug");
                array_push($tableValue,$slug);
                $stringType .=  "s";
            }

            if($type)
            {
                array_push($tableName,"animal_type");
                array_push($tableValue,$type);
                $stringType .=  "s";
            }

            if($expiry)
            {
                array_push($tableName,"expiry_date");
                array_push($tableValue,$expiry);
                $stringType .=  "s";
            }

            if($status)
            {
                array_push($tableName,"status");
                array_push($tableValue,$status);
                $stringType .=  "s";
            }

            if($description)
            {
                array_push($tableName,"description");
                array_push($tableValue,$description);
                $stringType .=  "s";
            }

            if($keyword)
            {
                array_push($tableName,"keyword_one");
                array_push($tableValue,$keyword);
                $stringType .=  "s";
            }

            array_push($tableValue,$uid);
            $stringType .=  "s";
            $updatePuppyDetails = updateDynamicData($conn,"variation"," WHERE product_id = ? ",$tableName,$tableValue,$stringType);
            if($updateProductDetails)
            {    
                $_SESSION['messageType'] = 1;
                echo "<script>alert('Data Updated and Stored !');window.location='../allProducts.php'</script>"; 
            }
            else
            {
            
                $_SESSION['messageType'] = 1;
                echo "<script>alert('Fail to Update Data !');window.location='../allProducts.php'</script>"; 
            }
        }
        else
        {
            echo "<script>alert('ERROR 1');window.location='../allProducts.php'</script>"; 
        }

    }
    else
    {
        echo "<script>alert('Fail to Update Data on puppy table!');window.location='../allProducts.php'</script>"; 
    }
}
else
{
    header('Location: ../index.php');
    // $_SESSION['messageType'] = 1;
    // header('Location: ../editProfile.php?type=1');
}

?>