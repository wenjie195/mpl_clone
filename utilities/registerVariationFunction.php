<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

function registerVariation($conn,$uid,$productId,$variation,$variationPrice,$variationStock,$category,$brand,$name,$sku,
$slug,$animalType,$expiryDate,$status,$description,$keywordOne)
{
     if(insertDynamicData($conn,"variation",array("uid","product_id","variation","variation_price","variation_stock",
"category","brand","name","sku","slug","animal_type","expiry_date","status","description","keyword_one"),
          array($uid,$productId,$variation,$variationPrice,$variationStock,$category,$brand,$name,$sku,
          $slug,$animalType,$expiryDate,$status,$description,$keywordOne),"sssssssssssssss") === null)
     {
          echo "gg";
          // header('Location: ../addReferee.php?promptError=1');
          //     promptError("error registering new account.The account already exist");
          //     return false;
     }
     else{    }
     return true;
}

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = md5(uniqid());
     $category= rewrite($_POST['update_category']);
     $brand = rewrite($_POST['update_brand']);
     $name = rewrite($_POST['update_name']);
     $sku = rewrite($_POST['update_sku']);
     $slug = rewrite($_POST['update_slug']);
     $animalType = rewrite($_POST['update_animal_type']);
     $expiryDate = rewrite($_POST['update_expiry_date']);
     $status = rewrite($_POST['update_status']);
     $description = rewrite($_POST['update_description']);
     $keywordOne = rewrite($_POST['update_keyword_one']);

     $productId= rewrite($_POST['update_product_uid']);
     $variation = rewrite($_POST['register_variation']);
     $variationPrice = rewrite($_POST['register_variation_price']);
     $variationStock = rewrite($_POST['register_variation_stock']);


        if(registerVariation($conn,$uid,$productId,$variation,$variationPrice,$variationStock,$category,$brand,$name,$sku,
        $slug,$animalType,$expiryDate,$status,$description,$keywordOne))
        {
            //$_SESSION['messageType'] = 1;
            //header('Location: ../addProductImage.php?uid='.$uid.'');
            $_SESSION['newVariation_uid'] = $uid;
            $_SESSION['newProduct_uid'] = $productId;
            header('Location: ../cropVariationImage.php');

        }

    // // FOR DEBUGGING 
     // echo "<br>";
     echo $uid."<br>";
     echo $productId."<br>";
     echo $category."<br>";
     echo $brand."<br>";
     echo $name."<br>";
     echo $sku."<br>";
     echo $slug."<br>";
     echo $animalType ."<br>";
     echo $expiryDate."<br>";
     echo $status."<br>";
     echo $description."<br>";
     echo $keywordOne."<br>";

}
else
{
     header('Location: ../index.php');
}

?>
