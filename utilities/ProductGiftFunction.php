<?php

require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/Product.php';

require_once dirname(__FILE__) . '/allNoticeModals.php';
require_once dirname(__FILE__) . '/databaseFunction.php';
require_once dirname(__FILE__) . '/generalFunction.php';

$uid = $_SESSION['uid'];

// function registerVariation($conn,$freeGift,$freeGiftDesc)
// {
//      if(insertDynamicData($conn,"product",array("free_gift","free_gift_description"),
//           array($freeGift,$freeGiftDesc),"ss") === null)
//      {
//           echo "gg";
          
//      }
//      else{    }
//      return true;
// }

if($_SERVER['REQUEST_METHOD'] == 'POST')
{
     $conn = connDB();

     $uid = rewrite($_POST['register_product_uid']);
     $freeGift = rewrite($_POST['register_free_gift']);
     $freeGiftDesc = rewrite($_POST['register_freeGift_desc']);

     $product = getProduct($conn," uid = ?   ",array("uid"),array($uid),"s");  

     if(!$product)
     {   
          $tableName = array();
          $tableValue =  array();
          $stringType =  "";
          //echo "save to database";
          if($freeGift)
          {
               array_push($tableName,"free_gift");
               array_push($tableValue,$freeGift);
               $stringType .=  "s";
          }
          if($freeGiftDesc)
          {
               array_push($tableName,"free_gift_description");
               array_push($tableValue,$freeGiftDesc);
               $stringType .=  "s";
          }
          array_push($tableValue,$uid);
          $stringType .=  "s";
          $passwordUpdated = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
          if($passwordUpdated)
          {
               $_SESSION['newProduct_uid'] = $uid;
               header('Location: ../cropGiftImage.php');
               //echo "success";
          }
          else
          {
               $_SESSION['messageType'] = 1;
               header('Location: ../productGift.php?type=2');
               //echo "e1";
          }
     }
     else
     {
          $_SESSION['messageType'] = 1;
          header('Location: ../productGift.php?type=3');
          //echo "e1";
     } 
     
     // if(registerVariation($conn,$freeGift,$freeGiftDesc))
     //    {
     //        //$_SESSION['messageType'] = 1;
     //        //header('Location: ../addProductImage.php?uid='.$uid.'');
     //        $_SESSION['newProduct_uid'] = $uid;
     //        //echo $_SESSION['newProduct_uid'] ;
     //        header('Location: ../cropGiftImage.php');

     //      // echo $uid ;
     //      // echo $freeGift ;
     //      // echo $freeGiftDesc ;

     //    }

}
else
{
     header('Location: ../index.php');
}

?>
