<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/1dbCon/dbCon2.php';
// require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/Services.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Our Partners" />
<title>Our Partners</title>
<meta property="og:description" content="Our Partners" />
<meta name="description" content="Our Partners" />
<meta name="keywords" content="Our Partners">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<script src="js/jquery-2.2.0.min.js"></script>
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>

      <div class="fix-filter width100 small-padding overflow">
            <h1 class="green-text user-title left-align-title">Partners</h1>

            <div class="filter-div">
            	<a class="open-filter2 filter-a green-a">Filter</a>
            </div>
      </div>

      <!-- Filter Modal -->
      <div id="filter-modal2" class="modal-css">

            <!-- Modal content -->
            <div class="modal-content-css big-filter-margin">
                  <span class="close-css close-filter2">&times;</span>

                  <div class="clear"></div>
                  <h2 class="green-text h2-title">Filter</h2>
                  <div class="green-border filter-border"></div>          				

                  <div class="filter-div-section">
                        <p class="filter-label filter-label3 green-filter">State</p>
                        <?php
                        $query = "
                        SELECT * FROM states
                        ";
                        $statement = $connect->prepare($query);
                        $statement->execute();
                        $result = $statement->fetchAll();
                        foreach($result as $row)
                        {
                        ?>
                        
                              <div class="filter-option">
                                    <label  class="filter-label filter-label2">
                                          <input type="checkbox" class="common_selector filter-option state" value="<?php echo $row['state_name']; ?>" > <?php echo $row['state_name']; ?><span class="checkmark"></span>
                                    </label>  
                              </div>                   

                        <?php    
                        }
                        ?>
                  </div>
              
            </div>
      </div>

      <div class="width100 small-padding overflow min-height-with-filter filter-distance">
            <div class="width103">     
                  <div class="filter_data"></div>
            </div>
      </div>
      <div class="clear"></div>

      <style>
            .animated.slideUp{
                  animation:none !important;}
            .animated{
                  animation:none !important;}
            .partner-a .hover1a{
                  display:none !important;}
            .partner-a .hover1b{
                  display:inline-block !important;}
      </style>

      <script>
            $(document).ready(function(){

                  filter_data();

                  function filter_data()
                  {
                  $('.filter_data').html('<div id="loading" style="" ></div>');
                  var action = 'filterdataGrooming.php';
                  var state = get_filter('state');
                  var services = get_filter('services');
                  $.ajax({
                        url:"filterdataGrooming.php",
                        method:"POST",
                        data:{action:action, state:state, services:services},
                        success:function(data){
                              $('.filter_data').html(data);
                        }
                  });
                  }

                  function get_filter(class_name)
                  {
                  var filter = [];
                  $('.'+class_name+':checked').each(function(){
                        filter.push($(this).val());
                  });
                  return filter;
                  }

                  $('.common_selector').click(function(){
                  filter_data();
                  });

            });
      </script>
<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</body>
</html>
