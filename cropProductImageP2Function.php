<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/Product.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$newProductUid = $_SESSION['newProduct_uid'];

$conn = connDB();
$timestamp = time();

if(isset($_POST["image"]))
{
	$data = $_POST["image"];

	$image_array_1 = explode(";", $data);

	$image_array_2 = explode(",", $image_array_1[1]);

	$data = base64_decode($image_array_2[1]);

	// $imageName = time() . '.png';
	$imageName = $newProductUid.time() . '.png';

	file_put_contents('uploads/'.$imageName, $data);

	if(isset($_POST["image"]))
	{
		$tableName = array();
		$tableValue =  array();
		$stringType =  "";
		//echo $imageName;

		if($imageName)
		{
		array_push($tableName,"image_two");
		array_push($tableValue,$imageName);
		$stringType .=  "s";
		}

		array_push($tableValue,$newProductUid);
		$stringType .=  "s";
		$uploadProductImageTwo = updateDynamicData($conn,"product"," WHERE uid = ? ",$tableName,$tableValue,$stringType);
		if($uploadProductImageTwo)
		{

			if(isset($_POST["image"]))
			{

				if($imageName)
				{
				?>
					<div class="preview-img-div">
						<img src=uploads/<?php echo $imageName  ?> class="width100" />
					</div>
				<?php
				?>
		
				<h4><?php echo "Image Two"; ?></h4>
			
				<a href="cropProductImageP3.php" class="red-link">
					<button class="green-button white-text clean2 edit-1-btn margin-auto">Next</button>
				</a>
		
				<?php
				}
				else{
					echo "FAIL Level 2";
				}
		
			}
			
		}
		else{
			echo "FAIL Level 2";
		}
	}
}
?>