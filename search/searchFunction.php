<?php
if (session_id() == "")
{
  session_start();
}
require_once dirname(__FILE__) . '/../1dbCon/dbCon.php';
// require_once dirname(__FILE__) . '/../sessionLoginChecker.php';

require_once dirname(__FILE__) . '/../classes/User.php';

require_once dirname(__FILE__) . '/../utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/../utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/../utilities/generalFunction.php';

$search = $_POST['search'];
$location = $_POST['location'];

if ($search) {
  header('location: '.$location.'?search='.$search.'');
}else {
  header('location: '.$location.'');
}
 ?>
