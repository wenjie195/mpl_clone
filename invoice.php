<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/ProductOrders.php';
require_once dirname(__FILE__) . '/classes/Invoice.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$date = date("Y-m-d");
$time = date("h:i a");

$id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
$orderUid = $id[0]->getId();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $conn = connDB();

    // $id = rewrite($_POST["order_id"]);
    // $username = rewrite($_POST["insert_username"]);
    $bankName = rewrite($_POST["insert_bankname"]);
    $bankAccountHolder = rewrite($_POST["insert_bankaccountholder"]);
    $bankAccountNo = rewrite($_POST["insert_bankaccountnumber"]);
    
    $uid = ($_POST['uid']);
   
    $subtotal = rewrite($_POST["subtotal"]);
    $total = rewrite($_POST["total"]);

    $payment_method = rewrite($_POST["payment_method"]);
    $payment_status = rewrite($_POST["payment_status"]);
    $shipping_status = rewrite($_POST["shipping_status"]);

    if(isset($_POST['next']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
    
        //echo "save to database";

        if($bankName)
        {
            array_push($tableName,"bank_name");
            array_push($tableValue,$bankName);
            $stringType .=  "s";
        }
        if($bankAccountHolder)
        {
            array_push($tableName,"bank_account_holder");
            array_push($tableValue,$bankAccountHolder);
            $stringType .=  "s";
        }
        if($bankAccountNo)
        {
            array_push($tableName,"bank_account_no");
            array_push($tableValue,$bankAccountNo);
            $stringType .=  "s";
        }
        if($subtotal)
        {
            array_push($tableName,"subtotal");
            array_push($tableValue,$subtotal);
            $stringType .=  "d";
        }
        if($total)
        {
            array_push($tableName,"total");
            array_push($tableValue,$total);
            $stringType .=  "d";
        }
        if($payment_method)
        {
            array_push($tableName,"payment_method");
            array_push($tableValue,$payment_method);
            $stringType .=  "s";
        }
        // if($payment_amount)
        // {
        //     array_push($tableName,"payment_amount");
        //     array_push($tableValue,$payment_amount);
        //     $stringType .=  "i";
        // }
        
        if($payment_status)
        {
            array_push($tableName,"payment_status");
            array_push($tableValue,$payment_status);
            $stringType .=  "s";
        }
        if($shipping_status)
        {
            array_push($tableName,"shipping_status");
            array_push($tableValue,$shipping_status);
            $stringType .=  "s";
        }

        // echo $uid. "<br>";
        // echo $name. "<br>";
        // echo $contactNo. "<br>";
        // echo $address_1. "<br>";
        // echo $address_2. "<br>";
        // echo $city ."<br>";
        // echo $zipcode ."<br>";
        // echo $state."<br>";
        // echo $country ."<br>";
        // echo count($tableValue);
    
        array_push($tableValue,$uid);
        $stringType .=  "s";
        $updateOrderDetails = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($updateOrderDetails)
        {
            //echo "<script>alert('Successfully added shipping details!');</script>";
        }
        else
        {
            echo "<script>alert('Fail to add shipping details!');window.location='../checkout.php'</script>"; 
        }
    }
    else
    {
        echo "<script>alert('ERROR 2');window.location='../checkout.php'</script>"; 
    }

         
}
else 
{
     header('Location: ../index.php');
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$userOrder = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$orderDetails = $userOrder[0];

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,2);
}else
{}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Invoice | Mypetslibrary" />
<title>Invoice | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
    <div class="sticky-tab menu-distance2">
        <div class="tab sticky-tab-tab">
            <button class="tablinks active hover1 tab-tab-btn" onclick="openTab(event, 'Cart')">
                <div class="green-dot"></div>
                <img src="img/cart-1.png" class="tab-icon hover1a" alt="Cart" title="Cart">
                <img src="img/cart-2.png" class="tab-icon hover1b" alt="Cart" title="Cart">
                <p class="tab-tab-p">In Cart</p>
                
            </button>
            <button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Ship')">
                <div class="green-dot"></div>
                <img src="img/to-ship-1.png" class="tab-icon hover1a" alt="To Ship" title="To Ship">
                <img src="img/to-ship-2.png" class="tab-icon hover1b" alt="To Ship" title="To Ship">        
                <p class="tab-tab-p">To Ship</p>
            </button>
            <button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Receive')">
                <div class="green-dot"></div>
                <img src="img/to-receive-1.png" class="tab-icon hover1a" alt="To Receive" title="To Receive">
                <img src="img/to-receive-2.png" class="tab-icon hover1b" alt="To Receive" title="To Receive">         
                <p class="tab-tab-p">To Receive</p>
            </button>
            <button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Received')">
                <div class="green-dot"></div>
                <img src="img/received-1.png" class="tab-icon hover1a" alt="Received" title="Received">
                <img src="img/received-2.png" class="tab-icon hover1b" alt="Received" title="Received">         
                <p class="tab-tab-p">Received</p>
            </button>        
        </div>
    </div>

    <div class="two-menu-space width100"></div>    
        <div class="width100 same-padding min-height4">
            <form method="POST" action="utilities/orderInformation.php" enctype="multipart/form-data">

                <input class="clean white-input two-box-input" type="hidden" id="order_id" name="order_id" value="<?php echo $id;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_username" name="insert_username" value="<?php echo $username;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_bankname" name="insert_bankname" value="<?php echo $bankName;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_bankaccountholder" name="insert_bankaccountholder" value="<?php echo $bankAccountHolder;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_bankaccountnumber" name="insert_bankaccountnumber" value="<?php echo $bankAccountNo;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_name" name="insert_name" value="<?php echo $name;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_contactNo" name="insert_contactNo" value="<?php echo $contactNo;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_address_1" name="insert_address_1" value="<?php echo $address_1;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_address_2" name="insert_address_2" value="<?php echo $address_2;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_city" name="insert_city" value="<?php echo $city;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_zipcode" name="insert_zipcode" value="<?php echo $zipcode;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_state" name="insert_state" value="<?php echo $state;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_country" name="insert_country" value="<?php echo $country;?>">


                <?php
                    if(isset($_POST['uid']))
                    {
                        $conn = connDB();
                        $orders = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['uid']),"s");
                    ?>

                    <div class="dual-div">
                        <p class="green-text top-text">Order No.</p>
                        <p class="bottom-text"><?php echo $orders[0]->getId();?></p>
                    </div>
                    <div class="dual-div second-dual-div">
                        <p class="green-text top-text">Paid On</p>
                        <p class="bottom-text"><?php echo $orders[0]->getDateCreated();?></p>
                    </div> 
                    <?php
                    }
                ?> 
                <!-- ---------------------------------------------------Imp--------------------------------------------------------- -->
                <div class="white-input-div payment-white-div">
                    <p class="payment-input-p">
                    <!-- Payment Method -->
                        <div class="clean edit-profile-input payment-input" type="hidden" id="payment_method" name="payment_method">
                            <input type="hidden" id="billPlz" value="<?php echo 'CreateABill.php?amount='.$_SESSION['total'] ?>" name="Online Banking">   
                        </div>
                    </p>
                    <!-- <a><img src="img/billplz.png" style="width:200px;" class="pointer opacity-hover"></a> -->
                </div>
                <!-- ---------------------------------------------------Imp--------------------------------------------------------- -->

                <?php
                if(isset($_POST['uid']))
                {
                    $orderId = $_POST['uid'];
                    $conn = connDB();
                    //$orders = getProductOrders($conn,"WHERE order_id = ? ", array("order_id") ,array($_POST['order_id']),"s");
                    $orders = getProductOrders($conn);
                    $order = getOrders($conn);
                    ?>
                    <div class="width100 overflow">
                        <p class="green-text top-text">Ordered Products</p>
                        <div class="table-scroll-div">
                            <table class="order-table">
                                <thead>	
                                    <tr>
                                        <th>No.</th>
                                        <th>Product</th>
                                        <th>Qty</th>
                                        <th class="price-column">Total Price (RM)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php 
                                $no=1;
                                for($cnt = 1;$cnt < count($orders) ;$cnt++){
                                    if($orders[$cnt]->getOrderId() == $orderId)
                                    {
                                        ?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $orders[$cnt]->getProductName();?></td>
                                            <td><?php echo $orders[$cnt]->getQuantity();?></td>
                                            <td class="price-column"><?php echo $orders[$cnt]->getTotalProductPrice();?></td>
                                        </tr>
                                        <?php
                                        $no++;
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                        <table class="price-table">
                            <tbody>
                            <?php
                                if(isset($_POST['uid']))
                                {
                                    $conn = connDB();
                                    $orders = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['uid']),"s");
                                    ?>
                                        <tr>
                                            <td>Subtotal</td>
                                            <td class="price-padding">RM<?php echo $orders[0]->getSubtotal();?>.00</td>
                                        </tr>
                                        <tr>
                                            <td>Shipping</td>
                                                <?php 
                                                    $subtotal=$orders[0]->getSubtotal();
                                                    if($subtotal<100){
                                                        $shippingFee = 30;
                                                    }
                                                    else{
                                                        $shippingFee = 0;
                                                    }
                                                ?>
                                            <td class="price-padding">RM<?php echo $shippingFee;?>.00</td>
                                        </tr>
                                        <tr>
                                            <td class="last-td"><b>Total</b></td>
                                            <td class="last-td price-padding"><b>RM<?php echo $orders[0]->getTotal();?>.00</b></td>
                                        </tr>
                            </tbody>
                        </table>                        
                        </div>
                        <?php
                    }
                    ?>
                <div class="clear"></div>
                <div class="width100 overflow some-margin-bottom">
                    <p class="green-text top-text">Delivery Address</p>
                    <p class="bottom-text"><b>Mr/Miss <?php echo $orders[0]->getName();?></b> (<?php echo $orders[0]->getContactNo();?>)</p>
                    <p class="bottom-text"><?php echo $orders[0]->getAddressLine1();?>,<br><?php echo $orders[0]->getCity();?>,
                    <?php echo $orders[0]->getZipcode();?>, <?php echo $orders[0]->getState();?>, <?php echo $orders[0]->getCountry();?></p>      	
                </div>
                <div class="clear"></div>
  
               <div style="display: none" id="showLater">
                  <div class="white-input-div payment-white-div">
                        <input required type="time" id="payment_time" name="payment_time" value="<?php echo $time ?>">
                  </div>
                </div>

                <?php
                }
                ?>

                <input type="hidden" value="PENDING" class="clean edit-profile-input payment-input" id="payment_status" name="payment_status">
                <input type="hidden" value="PENDING" class="clean edit-profile-input payment-input" id="shipping_status" name="payment_status">

                <div class="clear"></div>  
                <div class="width100 text-center">                                                                         
                    <button class="green-button checkout-btn clean" id="completeOrder">COMPLETE ORDER</button>
                </div>
            </form>
        
    </div>
    </div>

    <?php include 'js.php'; ?>

</body>

    <script>

    function la(src){
        window.location=src;
    }
    </script>
    <script type="text/javascript">
      $(document).ready(function(){
        $("#payment_method").change(function(){
          if ($("#payment_method").val() == 'CDM') {
            $("#showLater").slideDown(function(){
              $(this).show();
            });
          }
          else {
            $("#showLater").slideUp(function(){
              $(this).hide();
            });
          }
        });

        $("#completeOrder").click(function(){
          var sessName = '<?php echo 'CreateABill.php?amount='.$_SESSION['total'] ?>';
          var sessName2 = '<?php echo 'CreateABill.php' ?>';
          if ($("#payment_method").val() == 'CDM') {
            window.location = sessName2;
          }
          else {

              window.location = sessName;
          }
            });
        });
    </script>

<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.green-footer{
		display:none;}
</style>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>
<script>
$("#copy-referral-link").click(function(){
          var textArea = document.createElement("textarea");
          textArea.value = $('#linkCopy').val();
          document.body.appendChild(textArea);
          textArea.select();
          document.execCommand("Copy");
          textArea.remove();
          putNoticeJavascript("Copied!! ","");
      });
      
</script>
</html>