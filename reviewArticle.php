<?php
// require_once dirname(__FILE__) . '/sellerAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];
$userType = $_SESSION['usertype'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Review Article | Mypetslibrary" />
<title>Review Article | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library, pet, online pet store, pet seller, cat, kitten, dog, puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">

<?php include 'header.php'; ?>


<div class="width100 blog-big-div overflow min-height menu-distance2">

    <div class="blog-inner-div">

        <div class="blog-content">

            <?php
            if(isset($_POST['article_uid']))
            {
            $conn = connDB();
            $artUid = getArticles($conn,"WHERE uid = ? ", array("uid") ,array($_POST['article_uid']),"s");
            $artDetails = $artUid[0];
            ?>

                <img src="uploadsArticle/<?php echo $artDetails->getTitleCover();?>" class="cover-photo" alt="Blog Title" title="Blog Title"> 

                <h1 class="green-text user-title ow-margin-bottom-0"><?php echo $artDetails->getTitle();?></h1>

                <p class="small-blog-date"><?php echo $date = date("d/m/Y",strtotime($artDetails->getDateCreated()));?></p>

                <p class="article-paragraph">
                    <?php echo $artDetails->getParagraphOne();?>
                </p>

                <h1 class="green-text user-title ow-margin-bottom-0"></h1>
                <?php $status =  $artDetails->getDisplay();?>
            <?php
            }
            ?>

            <div class="clear"></div>

        </div>

        <div class="width100 text-center"></div>

        <!-- <?php
        if($userType == 0 && $status == 'Yes')
        {
        ?>
            <a href='adminEditArticle.php?id=<?php echo $artDetails->getUid();?>'>
                <div class="green-button white-text clean2 edit-1-btn margin-auto margin-bottom30">Edit</div>
            </a>
        <?php
        }
        elseif($userType != 0 && $status == 'Yes')
        {
        ?>
        <?php
        }
        else
        {
        ?>
            <a href='adminEditArticle.php?id=<?php echo $artDetails->getUid();?>'>
                <div class="green-button white-text clean2 edit-1-btn margin-auto margin-bottom30">Edit</div>
            </a>
        <?php
        }
        ?> -->
        
        <a href='adminEditArticle.php?id=<?php echo $artDetails->getUid();?>'>
            <div class="green-button white-text clean2 edit-1-btn margin-auto margin-bottom30">Edit</div>
        </a>

    </div>
    
</div>

<div class="clear"></div>

<?php include 'js.php'; ?>

</body>
</html>