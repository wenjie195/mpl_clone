<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Shipping.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();
$date = date("Y-m-d");
$time = date("h:i a");

$id = getOrders($conn," WHERE uid = ? ORDER BY date_created DESC LIMIT 1 ",array("uid"),array($uid),"s");
$orderUid = $id[0]->getId();

if($_SERVER['REQUEST_METHOD'] == 'POST'){
    $conn = connDB();
    
    $uid = ($_POST['uid']);

    $name = ($_POST["insert_name"]);
    //$_SESSION['name'] = $name;
    $contactNo = ($_POST["insert_contactNo"]);
    //$_SESSION['contact'] = $contactNo;
    $address_1 = ($_POST["insert_address_1"]);
    $address_2 = ($_POST["insert_address_2"]);
    $city = ($_POST["insert_city"]);
    $zipcode = ($_POST["insert_zipcode"]);
    $state = ($_POST["insert_state"]);
    $country = ($_POST["insert_country"]);
    $subtotal = ($_POST["subtotal"]);

    if(isset($_POST['shippingSubmit']))
    {   
        $tableName = array();
        $tableValue =  array();
        $stringType =  "";
    
        //echo "save to database";
        if($name)
        {
            array_push($tableName,"name");
            array_push($tableValue,$name);
            $stringType .=  "s";
        }
    
        if($contactNo)
        {
            array_push($tableName,"contactNo");
            array_push($tableValue,$contactNo);
            $stringType .=  "s";
        }
    
        if($address_1)
        {
            array_push($tableName,"address_line_1");
            array_push($tableValue,$address_1);
            $stringType .=  "s";
        }
    
        if($address_2)
        {
            array_push($tableName,"address_line_2");
            array_push($tableValue,$address_2);
            $stringType .=  "s";
        }
    
        if($city)
        {
            array_push($tableName,"city");
            array_push($tableValue,$city);
            $stringType .=  "s";
        }
    
        if($zipcode)
        {
            array_push($tableName,"zipcode");
            array_push($tableValue,$zipcode);
            $stringType .=  "s";
        }
    
        if($state)
        {
            array_push($tableName,"state");
            array_push($tableValue,$state);
            $stringType .=  "s";
        }
    
        if($country)
        {
            array_push($tableName,"country");
            array_push($tableValue,$country);
            $stringType .=  "s";
        }

        if($country)
        {
            array_push($tableName,"country");
            array_push($tableValue,$country);
            $stringType .=  "s";
        }

        if($subtotal)
        {
            array_push($tableName,"subtotal");
            array_push($tableValue,$subtotal);
            $stringType .=  "d";
        }

        // echo $uid. "<br>";
        // echo $name. "<br>";
        // echo $contactNo. "<br>";
        // echo $address_1. "<br>";
        // echo $address_2. "<br>";
        // echo $city ."<br>";
        // echo $zipcode ."<br>";
        // echo $state."<br>";
        // echo $country ."<br>";
        // echo count($tableValue);
    
        array_push($tableValue,$uid);
        $stringType .=  "s";
        $updateOrderDetails = updateDynamicData($conn,"orders"," WHERE id = ? ",$tableName,$tableValue,$stringType);
        if($updateOrderDetails)
        {
            // echo "<script>alert('Successfully added shipping details!');</script>";
        }
        else
        {
            echo "<script>alert('Fail to add shipping details!');window.location='../checkout.php'</script>"; 
        }
    }
    else
    {
        echo "<script>alert('ERROR 2');window.location='../checkout.php'</script>"; 
    }

         
}
else 
{
     header('Location: ../index.php');
}

$userRows = getUser($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$userDetails = $userRows[0];

$userOrder = getOrders($conn," WHERE uid = ? ",array("uid"),array($uid),"s");
$orderDetails = $userOrder[0];

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,2);
}else
{}

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>


<!DOCTYPE html>
<html lang="en">
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Pedigree Dentastix Dog Treats | Mypetslibrary" />
<title>Pedigree Dentastix Dog Treats | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet.
Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="description" content="Mypetslibrary - Pedigree Dentastix Dog Treats - Daily oral care treat for your pet. Unique tasty X-shaped treat clinically proven to reduce plaque. It has active ingredients zinc sulphate &amp; sodium trio polyphosphate that help in slowly down the rate of tartar build up." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
    <div class="sticky-tab menu-distance2">
        <div class="tab sticky-tab-tab">
            <button class="tablinks active hover1 tab-tab-btn" onclick="openTab(event, 'Cart')">
                <div class="green-dot"></div>
                <img src="img/cart-1.png" class="tab-icon hover1a" alt="Cart" title="Cart">
                <img src="img/cart-2.png" class="tab-icon hover1b" alt="Cart" title="Cart">
                <p class="tab-tab-p">In Cart</p>
                
            </button>
            <button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Ship')">
                <div class="green-dot"></div>
                <img src="img/to-ship-1.png" class="tab-icon hover1a" alt="To Ship" title="To Ship">
                <img src="img/to-ship-2.png" class="tab-icon hover1b" alt="To Ship" title="To Ship">        
                <p class="tab-tab-p">To Ship</p>
            </button>
            <button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Receive')">
                <div class="green-dot"></div>
                <img src="img/to-receive-1.png" class="tab-icon hover1a" alt="To Receive" title="To Receive">
                <img src="img/to-receive-2.png" class="tab-icon hover1b" alt="To Receive" title="To Receive">         
                <p class="tab-tab-p">To Receive</p>
            </button>
            <button class="tablinks hover1 tab-tab-btn" onclick="openTab(event, 'Received')">
                <div class="green-dot"></div>
                <img src="img/received-1.png" class="tab-icon hover1a" alt="Received" title="Received">
                <img src="img/received-2.png" class="tab-icon hover1b" alt="Received" title="Received">         
                <p class="tab-tab-p">Received</p>
            </button>        
        </div>
    </div>

    <div class="two-menu-space width100"></div>    
        <div class="width100 same-padding min-height4">
            <form method="POST" action="invoice.php" enctype="multipart/form-data">

                <div class="clear"></div>

                <p class="info-title spacing2"><b>Ship To</b></p>


                <input class="clean white-input two-box-input" type="hidden" id="order_id" name="order_id" value="<?php echo $id;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_username" name="insert_username" value="<?php echo $username;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_bankname" name="insert_bankname" value="<?php echo $bankName;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_bankaccountholder" name="insert_bankaccountholder" value="<?php echo $bankAccountHolder;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_bankaccountnumber" name="insert_bankaccountnumber" value="<?php echo $bankAccountNo;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_name" name="insert_name" value="<?php echo $name;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_contactNo" name="insert_contactNo" value="<?php echo $contactNo;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_address_1" name="insert_address_1" value="<?php echo $address_1;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_address_2" name="insert_address_2" value="<?php echo $address_2;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_city" name="insert_city" value="<?php echo $city;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_zipcode" name="insert_zipcode" value="<?php echo $zipcode;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_state" name="insert_state" value="<?php echo $state;?>">
                <input class="clean white-input two-box-input" type="hidden" id="insert_country" name="insert_country" value="<?php echo $country;?>">

                <p class="info-title spacing2"><b>PAYMENT</b></p>
                <p class="smaller-text">All transactions are secure and encrypted.</p>


                <div class="dual-input">
                    <p class="input-top-p">Bank</p>
                    <p class="no-input">Maybank</p>
                    <input type="hidden" id="insert_bankname" name="insert_bankname" value="Maybank">
                </div>
            
                <div class="dual-input second-dual-input">
                    <p class="input-top-p">Bank Account</p>
                    <p class="no-input">158082626261</p>
                    <input type="hidden" id="insert_bankaccountnumber" name="insert_bankaccountnumber" value="158082626261">
                </div>
                    
                <div class="clear"></div>
                <div class="dual-input">
                    <p class="input-top-p">Bank Acc. Holder</p>
                    <p class="no-input">Mypetslibrary</p>
                    <input type="hidden" id="insert_bankaccountholder" name="insert_bankaccountholder" value="Mypetslibrary">
                </div>

                <?php
                    if(isset($_POST['uid']))
                    {
                        $conn = connDB();
                        $orders = getOrders($conn,"WHERE id = ? ", array("id") ,array($_POST['uid']),"s");
                    ?>
                    <div class="dual-input second-dual-input">
                        <p class="input-top-p">Total</p>
                        <?php $orders[0]->getSubtotal();?>
                        <?php 
                            $subtotal=$orders[0]->getSubtotal();
                            if($subtotal<100){
                                $total = $subtotal + 30;
                            }
                            else{
                                $total = $subtotal + 0;
                            }
                        ?>
                        <p class="no-input">RM<?php echo $total;?>.00</p>
                        
                    </div>
                    <?php
                    }
                ?> 
                <div class="clear"></div>

                <!-- ---------------------------------------------------Imp--------------------------------------------------------- -->
                <div class="white-input-div payment-white-div">
                    <p class="payment-input-p">
                    <!-- Payment Method -->
                        <div class="clean edit-profile-input payment-input" type="hidden" id="payment_method" name="payment_method">
                            <input type="hidden" id="billPlz" value="<?php echo 'CreateABill.php?amount='.$_SESSION['total'] ?>" name="Online Banking">   
                        </div>
                    </p>
                    <!-- <a><img src="img/billplz.png" style="width:200px;" class="pointer opacity-hover"></a> -->
                </div>
                <!-- ---------------------------------------------------Imp--------------------------------------------------------- -->

                <div class="clear"></div>
                <div class="dual-input">
                    <input type="hidden" id="uid" name="uid" value="<?php echo $orderUid ?>">
                </div>

                <input type="hidden" value="PENDING" class="clean edit-profile-input payment-input" id="payment_status" name="payment_status">
                <input type="hidden" value="PENDING" class="clean edit-profile-input payment-input" id="shipping_status" name="shipping_status">
                <input type="hidden" value="BillPlz" class="clean edit-profile-input payment-input" id="payment_method" name="payment_method">
                <div class="clear"></div>
                
                <div class="right-status-div">
                <?php echo $productListHtml; ?>
                </div>

            </form>
        </div>
    </div>

    <?php include 'js.php'; ?>

</body>

    <script>

    function la(src){
        window.location=src;
    }
    </script>
 

<style>
	.animated.slideUp{
		animation:none !important;}
	.animated{
		animation:none !important;}
	.green-footer{
		display:none;}
</style>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

</html>