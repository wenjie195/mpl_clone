
<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon2.php';

if(isset($_POST["action"]))
{
	$query = "
		SELECT * FROM seller WHERE account_status = 'Active'
	";

	$statement = $connect->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll();
	$total_row_count = $statement->rowCount();

	if(isset($_POST["state"]))
	{
		$state_filter = implode("','", $_POST["state"]);
		$query .= "
		 AND state IN('".$state_filter."')
		";
	}
	if(isset($_POST["services"]))
	{
		$services_filter = implode("','", $_POST["services"]);
		$query .= "
		 AND services IN('".$services_filter."')
		";
	}

	$statement = $connect->prepare($query);
	$statement->execute();
	$result = $statement->fetchAll();
	$total_row = $statement->rowCount();
    $output = '';

	if($total_row > 0)
	{
		foreach($result as $row)
		{
			$uid= $row['uid'];

			$imageURL = './uploads/'.$row["company_logo"];
			
			if ($row['account_status'] == 'Active'){
				$output .= '
					<a href="petSellerDetails.php">
						<div id="'.$row['id'].'" class="shadow-white-box four-box-size opacity-hover pointer-div" value = "'.$row['slug'].'">
							<div class="width100 white-bg">
							<a href="uploads/'. $row['company_logo'] .'" class="progressive replace">
									<img src="img/pet-load300.jpg" class="preview width100 two-border-radius" alt="<?php echo '. $row['company_logo'] .'" title="<?php echo '. $row['company_logo'] .';?>" />
							</a>                     
							</div>
							<p align="center" class="width100 text-overflow slider-product-name">'. $row['company_name'] .'</p>
							
						</div>
					</a>
				';

			}		
		}
	}
	else
	{
		$output = '<h3>No Data Found</h3>';
	}
	echo $output;
}

?>
<script type="text/javascript">

var totalId = <?php echo $total_row_count + 1 ?>;
// alert(totalId);
for(var i=0; i< totalId; i++){

	$("#"+i+"").click(function(){
					var x = $(this).attr('value');
					location.href = "./petSellerDetails.php?id="+x+"";
				});

}
</script>