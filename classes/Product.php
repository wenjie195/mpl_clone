<?php  
class Product {
    /* Member variables */
    var $id,$uid,$category,$brand,$name,$sku,$slug,$animalType,$expiryDate,$status,$description,$keywordOne,$featuredProduct,$link,
    $imageOne,$imageTwo,$imageThree,$imageFour,$freeGift,$freeGiftImg,$freeGiftDescription,
    $dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param mixed $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return mixed
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param mixed $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getAnimalType()
    {
        return $this->animalType;
    }

    /**
     * @param mixed $animalType
     */
    public function setAnimalType($animalType)
    {
        $this->animalType = $animalType;
    }

    /**
     * @return mixed
     */
    public function getExpiryDate()
    {
        return $this->expiryDate;
    }

    /**
     * @param mixed $expiryDate
     */
    public function setExpiryDate($expiryDate)
    {
        $this->expiryDate = $expiryDate;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return mixed
     */
    public function getKeywordOne()
    {
        return $this->keywordOne;
    }

    /**
     * @param mixed $keywordOne
     */
    public function setKeywordOne($keywordOne)
    {
        $this->keywordOne = $keywordOne;
    }

    /**
     * @return mixed
     */
    public function getFeaturedProduct()
    {
        return $this->featuredProduct;
    }

    /**
     * @param mixed $featuredProduct
     */
    public function setFeaturedProduct($featuredProduct)
    {
        $this->featuredProduct = $featuredProduct;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getImageOne()
    {
        return $this->imageOne;
    }

    /**
     * @param mixed $imageOne
     */
    public function setImageOne($imageOne)
    {
        $this->imageOne = $imageOne;
    }

    /**
     * @return mixed
     */
    public function getImageTwo()
    {
        return $this->imageTwo;
    }

    /**
     * @param mixed $imageTwo
     */
    public function setImageTwo($imageTwo)
    {
        $this->imageTwo = $imageTwo;
    }

    /**
     * @return mixed
     */
    public function getImageThree()
    {
        return $this->imageThree;
    }

    /**
     * @param mixed $imageThree
     */
    public function setImageThree($imageThree)
    {
        $this->imageThree = $imageThree;
    }

    /**
     * @return mixed
     */
    public function getImageFour()
    {
        return $this->imageFour;
    }

    /**
     * @param mixed $imageFour
     */
    public function setImageFour($imageFour)
    {
        $this->imageFour = $imageFour;
    }

    /**
     * @return mixed
     */
    public function getFreeGift()
    {
        return $this->freeGift;
    }

    /**
     * @param mixed $freeGift
     */
    public function setFreeGift($freeGift)
    {
        $this->freeGift = $freeGift;
    }

    /**
     * @return mixed
     */
    public function getFreeGiftImg()
    {
        return $this->freeGiftImg;
    }

    /**
     * @param mixed $freeGiftImg
     */
    public function setFreeGiftImg($freeGiftImg)
    {
        $this->freeGiftImg = $freeGiftImg;
    }

    /**
     * @return mixed
     */
    public function getFreeGiftDescription()
    {
        return $this->freeGiftDescription;
    }

    /**
     * @param mixed $freeGiftDescription
     */
    public function setFreeGiftDescription($freeGiftDescription)
    {
        $this->freeGiftDescription = $freeGiftDescription;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getProduct($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","category","brand","name","sku","slug","animal_type","expiry_date","status",
    "description","keyword_one","featured_product","link","image_one","image_two","image_three","image_four",
    "free_gift","free_gift_img","free_gift_description","date_created","date_updated");//follow database

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"product"); //database name
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$category,$brand,$name,$sku,$slug,$animalType,$expiryDate,$status,$description,
        $keywordOne,$featuredProduct,$link,$imageOne,$imageTwo,$imageThree,$imageFour,$freeGift,$freeGiftImg,
        $freeGiftDescription,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Product;
            $class->setId($id);
            $class->setUid($uid);
            $class->setCategory($category);
            $class->setBrand($brand);
            $class->setName($name);
            $class->setSku($sku);
            $class->setSlug($slug);
            $class->setAnimalType($animalType);
            $class->setExpiryDate($expiryDate);
            $class->setStatus($status);
            $class->setDescription($description);
            $class->setKeywordOne($keywordOne);
            $class->setFeaturedProduct($featuredProduct);
            $class->setLink($link);
            $class->setImageOne($imageOne);
            $class->setImageTwo($imageTwo);
            $class->setImageThree($imageThree);
            $class->setImageFour($imageFour);
            $class->setFreeGift($freeGift);
            $class->setFreeGiftImg($freeGiftImg);
            $class->setFreeGiftDescription($freeGiftDescription);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}