<?php  
class Reptile {
    /* Member variables */
    var $id,$uid,$name,$sku,$slug,$keywordOne,$keywordTwo,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,$featuredSeller,
            $details,$detailsTwo,$detailsThree,$detailsFour,$link,$location,$imageOne,$imageTwo,$imageThree,$imageFour,$imageFive,$imageSix,$defaultImage,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * @param mixed $sku
     */
    public function setSku($sku)
    {
        $this->sku = $sku;
    }

    /**
     * @return mixed 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed 
     */
    public function getKeywordOne()
    {
        return $this->keywordOne;
    }

    /**
     * @param mixed $keywordOne
     */
    public function setKeywordOne($keywordOne)
    {
        $this->keywordOne = $keywordOne;
    }

    /**
     * @return mixed 
     */
    public function getKeywordTwo()
    {
        return $this->keywordTwo;
    }

    /**
     * @param mixed $keywordTwo
     */
    public function setKeywordTwo($keywordTwo)
    {
        $this->keywordTwo = $keywordTwo;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getAge()
    {
        return $this->age;
    }

    /**
     * @param mixed $age
     */
    public function setAge($age)
    {
        $this->age = $age;
    }

    /**
     * @return mixed
     */
    public function getVaccinated()
    {
        return $this->vaccinated;
    }

    /**
     * @param mixed $vaccinated
     */
    public function setVaccinated($vaccinated)
    {
        $this->vaccinated = $vaccinated;
    }

    /**
     * @return mixed
     */
    public function getDewormed()
    {
        return $this->dewormed;
    }

    /**
     * @param mixed $dewormed
     */
    public function setDewormed($dewormed)
    {
        $this->dewormed = $dewormed;
    }

    /**
     * @return mixed
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * @param mixed $gender
     */
    public function setGender($gender)
    {
        $this->gender = $gender;
    }

    /**
     * @return mixed
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param mixed $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return mixed
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param mixed $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getFeature()
    {
        return $this->feature;
    }

    /**
     * @param mixed $feature
     */
    public function setFeature($feature)
    {
        $this->feature = $feature;
    }

    /**
     * @return mixed
     */
    public function getBreed()
    {
        return $this->breed;
    }

    /**
     * @param mixed $breed
     */
    public function setBreed($breed)
    {
        $this->breed = $breed;
    }

    /**
     * @return mixed
     */
    public function getSeller()
    {
        return $this->seller;
    }

    /**
     * @param mixed $seller
     */
    public function setSeller($seller)
    {
        $this->seller = $seller;
    }

    /**
     * @return mixed
     */
    public function getFeaturedSeller()
    {
        return $this->featuredSeller;
    }

    /**
     * @param mixed $featuredSeller
     */
    public function setFeaturedSeller($featuredSeller)
    {
        $this->featuredSeller = $featuredSeller;
    }

    /**
     * @return mixed
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * @param mixed $details
     */
    public function setDetails($details)
    {
        $this->details = $details;
    }

    /**
     * @return mixed
     */
    public function getDetailsTwo()
    {
        return $this->detailsTwo;
    }

    /**
     * @param mixed $detailsTwo
     */
    public function setDetailsTwo($detailsTwo)
    {
        $this->detailsTwo = $detailsTwo;
    }

    /**
     * @return mixed
     */
    public function getDetailsThree()
    {
        return $this->detailsThree;
    }

    /**
     * @param mixed $detailsThree
     */
    public function setDetailsThree($detailsThree)
    {
        $this->detailsThree = $detailsThree;
    }

    /**
     * @return mixed
     */
    public function getDetailsFour()
    {
        return $this->detailsFour;
    }

    /**
     * @param mixed $detailsFour
     */
    public function setDetailsFour($detailsFour)
    {
        $this->detailsFour = $detailsFour;
    }

    /**
     * @return mixed
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param mixed $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return mixed
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * @param mixed $location
     */
    public function setLocation($location)
    {
        $this->location = $location;
    }

    /**
     * @return mixed
     */
    public function getImageOne()
    {
        return $this->imageOne;
    }

    /**
     * @param mixed $imageOne
     */
    public function setImageOne($imageOne)
    {
        $this->imageOne = $imageOne;
    }

    /**
     * @return mixed
     */
    public function getImageTwo()
    {
        return $this->imageTwo;
    }

    /**
     * @param mixed $imageTwo
     */
    public function setImageTwo($imageTwo)
    {
        $this->imageTwo = $imageTwo;
    }

    /**
     * @return mixed
     */
    public function getImageThree()
    {
        return $this->imageThree;
    }

    /**
     * @param mixed $imageThree
     */
    public function setImageThree($imageThree)
    {
        $this->imageThree = $imageThree;
    }

    /**
     * @return mixed
     */
    public function getImageFour()
    {
        return $this->imageFour;
    }

    /**
     * @param mixed $imageFour
     */
    public function setImageFour($imageFour)
    {
        $this->imageFour = $imageFour;
    }

    /**
     * @return mixed
     */
    public function getImageFive()
    {
        return $this->imageFive;
    }

    /**
     * @param mixed $imageFive
     */
    public function setImageFive($imageFive)
    {
        $this->imageFive = $imageFive;
    }

    /**
     * @return mixed
     */
    public function getImageSix()
    {
        return $this->imageSix;
    }

    /**
     * @param mixed $imageSix
     */
    public function setImageSix($imageSix)
    {
        $this->imageSix = $imageSix;
    }

    /**
     * @return mixed
     */
    public function getDefaultImage()
    {
        return $this->defaultImage;
    }

    /**
     * @param mixed $imageSix
     */
    public function setDefaultImage($defaultImage)
    {
        $this->defaultImage = $defaultImage;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getReptile($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","name","sku","slug","keyword_one","keyword_two","price","age","vaccinated","dewormed","gender","color","size","status","feature",
    "breed","seller","featured_seller","details","details_two","details_three","details_four","link","location","image_one","image_two","image_three","image_four",
        "image_five","image_six","default_image","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"reptile"); //database name
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$name,$sku,$slug,$keywordOne,$keywordTwo,$price,$age,$vaccinated,$dewormed,$gender,$color,$size,$status,$feature,$breed,$seller,
                $featuredSeller,$details,$detailsTwo,$detailsThree,$detailsFour,$link,$location,$imageOne,$imageTwo,$imageThree,$imageFour,$imageFive,$imageSix,$defaultImage,
                    $dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $class = new Reptile;
            $class->setId($id);
            $class->setUid($uid);
            $class->setName($name);
            $class->setSku($sku);
            $class->setSlug($slug);
            $class->setKeywordOne($keywordOne);
            $class->setKeywordTwo($keywordTwo);
            $class->setPrice($price);
            $class->setAge($age);
            $class->setVaccinated($vaccinated);
            $class->setDewormed($dewormed);
            $class->setGender($gender);
            $class->setColor($color);
            $class->setSize($size);
            $class->setStatus($status);
            $class->setFeature($feature);
            $class->setBreed($breed);
            $class->setSeller($seller);
            $class->setFeaturedSeller($featuredSeller);
            $class->setDetails($details);
            $class->setDetailsTwo($detailsTwo);
            $class->setDetailsThree($detailsThree);
            $class->setDetailsFour($detailsFour);
            $class->setLink($link);
            $class->setLocation($location);
            $class->setImageOne($imageOne);
            $class->setImageTwo($imageTwo);
            $class->setImageThree($imageThree);
            $class->setImageFour($imageFour);
            $class->setImageFive($imageFive);
            $class->setImageSix($imageSix);
            $class->setDefaultImage($defaultImage);
            $class->setDateCreated($dateCreated);
            $class->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$class);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
