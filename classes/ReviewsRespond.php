<?php  
class ReviewsRespond {
    /* Member variables */
    var $id,$uid,$username,$reviewUid,$likeAmount,$dislikeAmount,$dateCreated,$dateUpdated;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @param mixed $uid
     */
    public function setUid($uid)
    {
        $this->uid = $uid;
    }

    /**
     * @return mixed
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username)
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getReviewUid()
    {
        return $this->reviewUid;
    }

    /**
     * @param mixed $reviewUid
     */
    public function setReviewUid($reviewUid)
    {
        $this->reviewUid = $reviewUid;
    }

    /**
     * @return mixed
     */
    public function getLikeAmount()
    {
        return $this->likeAmount;
    }

    /**
     * @param mixed $likeAmount
     */
    public function setLikeAmount($likeAmount)
    {
        $this->likeAmount = $likeAmount;
    }

    /**
     * @return mixed
     */
    public function getDislikeAmount()
    {
        return $this->dislikeAmount;
    }
    
    /**
     * @param mixed $dislikeAmount
     */
    public function setDislikeAmount($dislikeAmount)
    {
        $this->dislikeAmount = $dislikeAmount;
    }

    /**
     * @return mixed
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * @param mixed $dateCreated
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;
    }

    /**
     * @return mixed
     */
    public function getDateUpdated()
    {
        return $this->dateUpdated;
    }

    /**
     * @param mixed $dateUpdated
     */
    public function setDateUpdated($dateUpdated)
    {
        $this->dateUpdated = $dateUpdated;
    }

}

function getReveiewsRespond($conn,$whereClause = null,$queryColumns = null,$queryValues = null,$queryTypes = null){
    $dbColumnNames = array("id","uid","username","review_uid","like_amount","dislike_amount","date_created","date_updated");

    $sql = sqlSelectSimpleBuilder($dbColumnNames,"reviews_respond");
    if($whereClause){
        $sql .= $whereClause;
    }

    if($stmt = $conn->prepare($sql)){
        /*
             Binds variables to prepared statement

             i    corresponding variable has type integer
             d    corresponding variable has type double
             s    corresponding variable has type string
             b    corresponding variable is a blob and will be sent in packets
        */

        if($queryColumns&&$queryTypes&&$queryValues){
            $stmt = returnStmtWithDynamicBinding($stmt,$queryValues,$queryTypes);
        }

//        $stmt->bind_param('s',$queryValues[0]);

        /* execute query */
        $stmt->execute();

        /* Store the result (to get properties) */
        $stmt->store_result();

        /* Get the number of rows */
        $num_of_rows = $stmt->num_rows;

        /* Bind the result to variables */
        $stmt->bind_result($id,$uid,$username,$reviewUid,$likeAmount,$dislikeAmount,$dateCreated,$dateUpdated);

        $resultRows = array();
        while ($stmt->fetch()) {
            $user = new ReviewsRespond;
            $user->setId($id);
            $user->setUid($uid);
            $user->setUsername($username);

            $user->setReviewUid($reviewUid);

            $user->setLikeAmount($likeAmount);
            $user->setDislikeAmount($dislikeAmount);
            $user->setDateCreated($dateCreated);
            $user->setDateUpdated($dateUpdated);
          
            array_push($resultRows,$user);
        }

        /* free results */
        $stmt->free_result();

        /* close statement */
        $stmt->close();

        if($num_of_rows <= 0){
            return null;
        }else{
            return $resultRows;
        }
    }else{
//        echo "Prepare Error: ($conn->errno) $conn->error";
        return null;
    }
}
