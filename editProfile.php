<?php
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';

require_once dirname(__FILE__) . '/classes/User.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>

<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Edit Profile" />
<title>Edit Profile</title>
<meta property="og:description" content="Edit Profile" />
<meta name="description" content="Edit Profile" />
<meta name="keywords" content="Edit Profile">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
	<div class="width100 same-padding overflow min-height menu-distance2">
    	<p class="review-product-name">Edit Profile</p>
 		<!-- <form> -->
        <form method="POST" action="utilities/editProfileFunction.php">
            <div class="dual-input">
                <p class="input-top-p">Name</p>
                <input class="input-name clean input-textarea" type="text" placeholder="Name" value="<?php echo $userData->getName();?>" name="update_name" id="update_name" required>   
            </div>

            <!-- <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p">Gender</p>     
                <select class="input-name clean" id="update_gender" name="update_gender">
                    <option value="Female" name="Female">Female</option>
                    <option value="Male" name="Male">Male</option>
                </select> 
            </div>         -->

            <div class="dual-input second-dual-input">
                <!-- <p class="input-top-p">Birthday</p> -->
                <p class="input-top-p">Birthday <?php echo $userData->getBirthday();?> </p>
                <input type="date"   class="input-name clean" id="update_dob" name="update_dob">     
            </div>

            <div class="clear"></div>

            <div class="dual-input">
                <p class="input-top-p">New Phone No.</p>
                <input class="input-name clean" type="text" placeholder="New Phone Number" value="<?php echo $userData->getPhoneNo();?>" id="update_phone" name="update_phone">   
            </div>

            <div class="dual-input second-dual-input">
                <p class="input-top-p">New Email</p>
                <input class="input-name clean" type="text" placeholder="Key in New Email" value="<?php echo $userData->getEmail();?>" id="update_email" name="update_email"> 
            </div>
        
            <div class="clear"></div>

            <div class="width100 overflow text-center">     
                <button class="green-button white-text clean2 edit-1-btn margin-auto">Submit</button>
            </div>
        </form>
    </div>
    
<div class="clear"></div>

<?php include 'js.php'; ?>
<?php include 'stickyDistance.php'; ?>
<?php include 'stickyFooter.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Profile Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            $messageType = "Fail to update profile";
        }
        else if($_GET['type'] == 3)
        {
            // $messageType = "User details is not available !!";
            $messageType = "ERROR !!";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>