<?php
if(isset($_SESSION['uid']))
{
?>

    <?php
    if($_SESSION['usertype'] == 0)
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"></a>
                    </div>
                    <div class="right-menu-div float-right">
                        <a href="adminDashboard.php" class="menu-padding black-to-white text-menu-a">
                            Dashboard
                        </a>                     
                        <div class="dropdown hover1 menu-margin">            
                            <a   class="menu-padding black-to-white text-menu-a">
                                Homepage <img src="img/dropdown.png" class="dropdown-img3">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        
                                        <!--<p class="dropdown-p"><a href="homeSettings.php"  class="menu-padding dropdown-a">Homepage Settings</a></p>-->
                                        <p class="dropdown-p"><a href="slider.php"  class="menu-padding dropdown-a">Slider</a></p>
                                        <!--<p class="dropdown-p"><a href="featuredProducts.php"  class="menu-padding dropdown-a">Featured Products</a></p>-->
                                        <p class="dropdown-p"><a href="featuredPartners.php"  class="menu-padding dropdown-a">Featured Partners</a></p>                                
                            </div>                
                        </div>
  
                        <div class="dropdown hover1">
                            <a  class="menu-margin menu-padding hover1 black-to-white  text-menu-a">
                                        Product <img src="img/dropdown2.png" class="dropdown-img3 hover1a"><img src="img/dropdown.png" class="dropdown-img3 hover1b">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <p class="dropdown-p"><a href="addPuppy.php"  class="menu-padding dropdown-a">Add Product</a></p>
                                        <p class="dropdown-p"><a href="allPuppies.php"  class="menu-padding dropdown-a">View Product</a></p>
                                        <!-- <p class="dropdown-p"><a href="addKitten.php"  class="menu-padding dropdown-a">Add Kitten</a></p>
                                        <p class="dropdown-p"><a href="allKittens.php"  class="menu-padding dropdown-a">View Kittens</a></p> -->
                            </div>
                        </div>  

                        <a href="seller.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Seller
                        </a>                            
                        <a href="allUsers.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            User
                        </a>  
                         <a href="reviewSummary.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Review
                        </a>                                                   
                         <a href="blogSummary.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Blog
                        </a>                 
                        <div class="dropdown hover1">
                            <a  class="menu-margin menu-padding">
                                      
                                        <img src="img/settings2.png" class="menu-img" alt="Settings" title="Settings">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <p class="dropdown-p"><a href="adminChangeEmail.php"  class="menu-padding dropdown-a">Change Email</a></p>
                                        <p class="dropdown-p"><a href="adminChangePassword.php"  class="menu-padding dropdown-a">Change Password</a></p>
                                        <p class="dropdown-p"><a href="logoutAdminSeller.php"  class="menu-padding dropdown-a">Logout</a></p>  
                            </div>
                        </div>                  
                            
                            
                            <div id="dl-menu" class="dl-menuwrapper">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                        <li><a href="adminDashboard.php">Dashboard</a></li>
                                        <li><a href="homeSettings.php">Homepage Settings</a></li>
                                        <li><a href="petSummary.php" class="mini-li">Pet Summary</a></li>
                                        <li><a href="seller.php" class="mini-li">All Sellers</a></li>
                                        <li><a href="allUsers.php"  class="mini-li">All Users</a></li>  
                                      
                                        <!--<li><a href="productSummary.php" class="mini-li">Product Summary</a>-->                                                   
                                        <li><a href="reviewSummary.php"  class="mini-li">Review Summary</a></li>   
                                        <li><a href="blogSummary" class="mini-li">Blog Summary</a></li>
                                                                         
                                        <li><a href="adminChangeEmail.php" class="mini-li">Change Email</a></li>
                                        <li><a href="adminChangePassword.php"  class="mini-li">Change Password</a></li>                                                                      
                                        <li  class="last-li"><a href="logoutAdminSeller.php">Logout</a></li>
                                </ul>
                            </div>       
                        
                                                
                    </div>
                </div>

        </header>

    <?php
    }
    elseif($_SESSION['usertype'] == 1)
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color logged-menu" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"></a>
                    </div>
                    <div class="right-menu-div float-right text-menu-right">
		
                        <a href="index.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Home
                        </a>   
                        <a href="profile.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Profile
                        </a> 

                        <!--<a href="malaysia-pet-food-toy-product.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Products
                        </a>   -->             
                        
                        <a href="malaysia-pet-blog.php" class="menu-padding black-to-white menu-margin text-menu-a">
                            Tips
                        </a>              
                        <a href="logout.php"  class="menu-padding black-to-white menu-margin text-menu-a">
                            Logout
                        </a>                                                                  
                            <div id="dl-menu" class="dl-menuwrapper">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">

                                        <li><a href="profile.php" class="mini-li">Profile</a></li>
                                        <li><a href="index.php" class="mini-li">Home</a></li>                            
                                        <!--<li><a href="malaysia-pet-food-toy-product.php"  class="mini-li">Products</a></li>-->   
                                        <li><a href="malaysia-pet-blog.php" class="mini-li">Pawsome Tips</a></li>
                                        <li><a href="logout.php" class="mini-li">Logout</a></li>
                                </ul>
                            </div>        
                        
                                                
                    </div>
                </div>

        </header>

    <?php
    }
    elseif($_SESSION['usertype'] == 2)
    {
    ?>

        <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
                <div class="big-container-size hidden-padding" id="top-menu">
                    <div class="float-left left-logo-div">
                        <a href="index.php"></a>
                    </div>
                    <div class="right-menu-div float-right">
                
                            <a href="sellerDashboard.php"   class="menu-padding hover1">
                                <img src="img/home.png" class="menu-img hover1a" alt="Dashboard" title="Dashboard">
                                <img src="img/home2.png" class="menu-img hover1b" alt="Dashboard" title="Dashboard">
                            </a>


                        <div class="dropdown hover1">
                            <a  class="menu-margin menu-padding hover1">
                                        <img src="img/seller.png" class="menu-img hover1a" alt="Seller" title="Seller">
                                        <img src="img/seller2.png" class="menu-img hover1b" alt="Seller" title="Seller">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <p class="dropdown-p"><a href="profilePreview.php"  class="menu-padding dropdown-a">Profile</a></p>
                                        <p class="dropdown-p"><a href="editSellerProfile.php"  class="menu-padding dropdown-a">Edit Profile</a></p>
                            </div>
                        </div>  
                                                   
                        <div class="dropdown hover1">
                            <a  class="menu-margin menu-padding hover1">
                                        <img src="img/article.png" class="menu-img hover1a" alt="Article" title="Article">
                                        <img src="img/article2.png" class="menu-img hover1b" alt="Article" title="Article">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <p class="dropdown-p"><a href="sellerBlogSummary.php"  class="menu-padding dropdown-a">Blog Summary</a></p>
                                        <p class="dropdown-p"><a href="sellerPendingArticle.php"  class="menu-padding dropdown-a">Pending Article</a></p>
                                        <p class="dropdown-p"><a href="sellerApprovedArticle.php"  class="menu-padding dropdown-a">Approved Article</a></p>
                                        <p class="dropdown-p"><a href="sellerRejectedArticle.php"  class="menu-padding dropdown-a">Rejected Article</a></p>
                                        <p class="dropdown-p"><a href="sellerReportedArticle.php"  class="menu-padding dropdown-a">Reported Article</a></p>
                                        <p class="dropdown-p"><a href="sellerAddArticle.php"  class="menu-padding dropdown-a">Write New Article</a></p> 
                            </div>
                        </div>                    
                        <div class="dropdown hover1">
                            <a  class="menu-margin menu-padding hover1">
                                        <img src="img/settings.png" class="menu-img hover1a" alt="Settings" title="Settings">
                                        <img src="img/settings2.png" class="menu-img hover1b" alt="Settings" title="Settings">
                            </a>
                            <div class="dropdown-content yellow-dropdown-content">
                                        <!-- <p class="dropdown-p"><a href="adminChangeEmail.php"  class="menu-padding dropdown-a">Change Email</a></p>
                                        <p class="dropdown-p"><a href="adminChangePassword.php"  class="menu-padding dropdown-a">Change Password</a></p> -->
                                        <p class="dropdown-p"><a href="sellerChangeEmail.php"  class="menu-padding dropdown-a">Change Email</a></p>
                                        <p class="dropdown-p"><a href="sellerChangePassword.php"  class="menu-padding dropdown-a">Change Password</a></p>
                                        <p class="dropdown-p"><a href="logoutAdminSeller.php"  class="menu-padding dropdown-a">Logout</a></p> 
                            </div>
                        </div>                  
                            
                            
                            <div id="dl-menu" class="dl-menuwrapper">
                                <button class="dl-trigger">Open Menu</button>
                                <ul class="dl-menu">
                                        <li><a href="sellerDashboard.php">Dashboard</a></li>
                                        <li><a href="profilePreview.php" class="mini-li">Profile</a></li>
                                        <li><a href="sellerPetSummary.php" class="mini-li">Pet Summary</a></li>
                                        
                                        <li><a href="sellerBlogSummary" class="mini-li">Blog Summary</a></li>                           
                                        <!-- <li><a href="adminChangeEmail.php" class="mini-li">Change Email</a></li>
                                        <li><a href="adminChangePassword.php"  class="mini-li">Change Password</a></li>           -->
                                        <li><a href="sellerChangeEmail.php" class="mini-li">Change Email</a></li>
                                        <li><a href="sellerChangePassword.php"  class="mini-li">Change Password</a></li>                                                               
                                        <li  class="last-li"><a href="logoutAdminSeller.php">Logout</a></li>
                                </ul>
                            </div>         
                        
                                                
                    </div>
                </div>

        </header>

    <?php
    }
    ?>

<?php
}
else
{
?>

    <div id="fb-root"></div>
    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v6.0&appId=2101026363514817&autoLogAppEvents=1"></script>
    <header id="header" class="header header--fixed same-padding header1 menu-color" role="banner">
            <div class="big-container-size hidden-padding" id="top-menu">
                <div class="float-left left-logo-div">
                    <a href="index.php"></a>
                </div>
                <div class="right-menu-div float-right text-menu-right">
                <a href="index.php" class="menu-padding black-to-white text-menu-a">
                        Home
                    </a>            
                    <!--<a href="malaysia-pet-food-toy-product-open.php" class="menu-padding black-to-white menu-margin text-menu-a">
                        Products
                    </a>  -->
                    <a href="malaysia-pet-blog.php" class="menu-padding black-to-white menu-margin text-menu-a">
                        Tips 
                    </a>            
                    <a class="menu-padding black-to-white open-signup menu-margin text-menu-a">
                        Sign Up
                    </a>
                    <a  class="menu-padding black-to-white open-login menu-margin text-menu-a">
                        Login
                    </a>                                                                  
                        <div id="dl-menu" class="dl-menuwrapper">
                            <button class="dl-trigger">Open Menu</button>
                            <ul class="dl-menu">
                                    <li><a class="open-signup mini-li">Sign Up</a></li>
                                    <li><a class="open-login mini-li">Login</a></li>
                                    <li><a href="index.php" class="mini-li">Home</a></li>
                                    <li><a href="malaysia-pet-blog.php" class="mini-li">Daily Tips</a></li>
                            </ul>
                        </div>         
                    
                                            
                </div>
            </div>

    </header>

<?php
}
?>