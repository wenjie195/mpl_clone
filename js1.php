<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Variation1.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$conn = connDB();

// if($_SERVER['REQUEST_METHOD'] == 'POST'){
//   //addToCart();
//   //header('Location: ./cart.php');
// }

$variation = getVariation($conn);

if(isset($_POST['product']))
{
    $conn = connDB();
    $products = getProduct($conn," WHERE id = ? ",array("id"),array($_POST['product_id']),"s");
    $product = $products[0];
    $uid = $product->getUid();
}

$productListHtml = "";

if(isset($_SESSION['shoppingCart']) && $_SESSION['shoppingCart']){
    $productListHtml = getShoppingCart($conn,1,true);
}else{
    if(isset($_POST['product-list-quantity-input'])){
        $productListHtml = createProductList($variation,1,$_POST['product-list-quantity-input']);
    }else{
        $productListHtml = createProductList($variation,$uid);
    }
}

$conn->close();
?>

<!-- Product Filter Modal -->
<div id="variation-modal" class="modal-css">

  <!-- Modal content -->
  <div class="modal-content-css filter-modal variation-modal-css">
    <span class="close-css close-variation">&times;</span>
    <h2 class="green-text h2-title">Amount and Variation</h2>
    <div class="green-border filter-border"></div>
	<div class="clear"></div>
    <div class="variation-left-div">
        <div class="variation-product-preview">
        <div  id="Variation1" class="tabcontent block2">
            <img src="img/product-1.jpg" class="variation-img" alt="Product Name" title="Product Name"  />
        </div>
        <div  id="Variation2" class="tabcontent">
            <img src="img/product-2.jpg" class="variation-img" alt="Product Name" title="Product Name"  />
        </div>
        <div  id="Variation3" class="tabcontent">
            <img src="img/product-3.jpg" class="variation-img" alt="Product Name" title="Product Name"  />
        </div>
        <div  id="Variation4" class="tabcontent">
            <img src="img/product-4.jpg" class="variation-img" alt="Product Name" title="Product Name"  />    
        </div> 
        
        </div>
    </div>
    <!-- <form method="POST"> -->
      <?php echo $productListHtml; ?>
    <!-- </form> -->
</div>

<script>
function myFunction1() {
  $variation = document.getElementById("variation1-input").value;
  $quantity = +($variation) + +5; //since it starts from 0 ned to add 5
  document.getElementById("quantity1").innerHTML=$quantity;
  $variationPrice = document.getElementById("price1").innerHTML;
  document.getElementById("pricy1").innerHTML=$variationPrice;
  $totalPrice=0;
  $totalPrice = $quantity * $variationPrice;
  document.getElementById("totalPrice1").innerHTML=$totalPrice;
}

function myFunction2() {
  $variation = document.getElementById("variation2-input").value;
  $quantity = +($variation) + +4;
  document.getElementById("quantity2").innerHTML=$quantity;
  $variationPrice = document.getElementById("price2").innerHTML;
  document.getElementById("pricy2").innerHTML=$variationPrice;
  $totalPrice=0;
  $totalPrice = $quantity * $variationPrice;
  document.getElementById("totalPrice2").innerHTML=$totalPrice;
}

function myFunction3() {
  $variation = document.getElementById("variation3-input").value;
  $quantity = +($variation) + +3;
  document.getElementById("quantity3").innerHTML=$quantity;
  $variationPrice = document.getElementById("price3").innerHTML;
  document.getElementById("pricy3").innerHTML=$variationPrice;
  $totalPrice = $quantity * $variationPrice;
  document.getElementById("totalPrice3").innerHTML=$totalPrice;
}

function myFunction4() {
  $variation = document.getElementById("variation4-input").value;
  $quantity = +($variation) + +2;
  document.getElementById("quantity4").innerHTML=$quantity;
  $variationPrice = document.getElementById("price4").innerHTML;
  document.getElementById("pricy4").innerHTML=$variationPrice;
  $totalPrice = $quantity * $variationPrice;
  document.getElementById("totalPrice4").innerHTML=$totalPrice;
}
</script>

<script type="text/javascript" src="js/jquery-2.2.1.min.js"></script>
<script src="js/jquery.fancybox.js" type="text/javascript"></script>  
<!-- filter dropdown script -->
<script type="text/javascript">
$('#breedGroup').change(function() {
    $('#breedGroupDiv').toggle();
});
$('#color').change(function() {
    $('#colorDiv').toggle();
});
$('#gender').change(function() {
    $('#genderDiv').toggle();
});
$('#state').change(function() {
    $('#stateDiv').toggle();
});
$('#partnerState').change(function() {
    $('#partnerStateDiv').toggle();
});
$('#price').change(function() {
    $('#priceDiv').toggle();
});
$('#rating').change(function() {
    $('#ratingDiv').toggle();
});
$('#services').change(function() {
    $('#servicesDiv').toggle();
});
$('#animalType').change(function() {
    $('#animalTypeDiv').toggle();
});
$('#brand').change(function() {
    $('#brandDiv').toggle();
});
$('#category').change(function() {
    $('#categoryDiv').toggle();
});
$('#productprice').change(function() {
    $('#productpriceDiv').toggle();
});
$('#variation1').change(function() {
    $('#variation1Div').toggle();
});
$('#variation2').change(function() {
    $('#variation2Div').toggle();
});
$('#variation3').change(function() {
    $('#variation3Div').toggle();
});
$('#variation4').change(function() {
    $('#variation4Div').toggle();
});
$('#voucher').change(function() {
    $('#voucherDiv').toggle();
});
</script>  
<script type="text/javascript" src="js/jquery-ui.js"></script>  
  <script type="text/javascript">
  
  $(function() {
    $( "#slider-range" ).slider({
      range: true,
      min: 0,
      max: 10000,
      values: [ 1000, 5000 ],
      slide: function( event, ui ) {
        $( "#amount" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		$( "#amount1" ).val(ui.values[ 0 ]);
		$( "#amount2" ).val(ui.values[ 1 ]);
      }
    });
    $( "#amount" ).html( "$" + $( "#slider-range" ).slider( "values", 0 ) +
     " - $" + $( "#slider-range" ).slider( "values", 1 ) );
  });
  </script>
  <script type="text/javascript">
  
  $(function() {
    $( "#slider-range2" ).slider({
      range: true,
      min: 0,
      max: 10000,
      values: [ 0, 10000 ],
      slide: function( event, ui ) {
        $( "#amounta" ).html( "$" + ui.values[ 0 ] + " - $" + ui.values[ 1 ] );
		$( "#amount3" ).val(ui.values[ 0 ]);
		$( "#amount4" ).val(ui.values[ 1 ]);
      }
    });
    $( "#amounta" ).html( "$" + $( "#slider-range2" ).slider( "values", 0 ) +
     " - $" + $( "#slider-range2" ).slider( "values", 1 ) );
  });
  </script>  
<script src="js/bootstrap.min.js" type="text/javascript"></script>    
<script src="js/headroom.js"></script>

<script>
    (function() {
        var header = new Headroom(document.querySelector("#header"), {
            tolerance: 5,
            offset : 205,
            classes: {
              initial: "animated",
              pinned: "slideDown",
              unpinned: "slideUp"
            }
        });
        header.init();
    
    }());
</script>
 
	<script>
    // Cache selectors
    var lastId,
        topMenu = $("#top-menu"),
        topMenuHeight = topMenu.outerHeight(),
        // All list items
        menuItems = topMenu.find("a"),
        // Anchors corresponding to menu items
        scrollItems = menuItems.map(function(){
          var item = $($(this).attr("href"));
          if (item.length) { return item; }
        });
    
    // Bind click handler to menu items
    // so we can get a fancy scroll animation
    menuItems.click(function(e){
      var href = $(this).attr("href"),
          offsetTop = href === "#" ? 0 : $(href).offset().top-topMenuHeight+1;
      $('html, body').stop().animate({ 
          scrollTop: offsetTop
      }, 500);
      e.preventDefault();
    });
    
    // Bind to scroll
    $(window).scroll(function(){
       // Get container scroll position
       var fromTop = $(this).scrollTop()+topMenuHeight;
       
       // Get id of current scroll item
       var cur = scrollItems.map(function(){
         if ($(this).offset().top < fromTop)
           return this;
       });
       // Get the id of the current element
       cur = cur[cur.length-1];
       var id = cur && cur.length ? cur[0].id : "";
                      
    });
    </script>
<script type="text/javascript" src="js/lightslider.js"></script>     
    <script>
    	 $(document).ready(function() {
			$("#content-slider").lightSlider({
                loop:false,
                keyPress:true
            });
            $('#image-gallery').lightSlider({
                gallery:true,
                item:1,
                thumbItem:9,
                slideMargin: 0,
                speed:500,
                auto:false,
                loop:true,
                onSliderLoad: function() {
                    $('#image-gallery').removeClass('cS-hidden');
                }  
            });
		});
    </script>    
<!--- Modal Box --->
<script>
var forgotmodal = document.getElementById("forgot-modal");
var signupmodal = document.getElementById("signup-modal");
var loginmodal = document.getElementById("login-modal");
var forgot2modal = document.getElementById("forgot2-modal");
var filtermodal = document.getElementById("filter-modal");
var socialmodal = document.getElementById("social-modal");
var reportmodal = document.getElementById("report-modal");
var reviewmodal = document.getElementById("review-modal");
var partnerfiltermodal = document.getElementById("partnerfilter-modal");
var productfiltermodal = document.getElementById("productfilter-modal");
var variationmodal = document.getElementById("variation-modal");
var ccvmodal = document.getElementById("ccv-modal");
var urlmodal = document.getElementById("url-modal");
var descmodal = document.getElementById("desc-modal");
var tutorialmodal = document.getElementById("tutorial-modal");
var confirmmodal = document.getElementById("confirm-modal");


var openforgot = document.getElementsByClassName("open-forgot")[0];
var openforgot2 = document.getElementsByClassName("open-forgot2")[0];
var opensignup = document.getElementsByClassName("open-signup")[0];
var opensignup1 = document.getElementsByClassName("open-signup")[1];
var opensignup2 = document.getElementsByClassName("open-signup")[2];
var opensignup3 = document.getElementsByClassName("open-signup")[3];
var openlogin = document.getElementsByClassName("open-login")[0];
var openlogin1 = document.getElementsByClassName("open-login")[1];
var openlogin2 = document.getElementsByClassName("open-login")[2];
var openlogin3 = document.getElementsByClassName("open-login")[3];
var openlogin4 = document.getElementsByClassName("open-login")[4];
var openfilter = document.getElementsByClassName("open-filter")[0];
var opensocial = document.getElementsByClassName("open-social")[0];
var openreport = document.getElementsByClassName("open-report")[0];
var openreport1 = document.getElementsByClassName("open-report")[1];
var openreport2 = document.getElementsByClassName("open-report")[2];
var openreview = document.getElementsByClassName("open-review")[0];
var openreview1 = document.getElementsByClassName("open-review")[1];
var openpartnerfilter = document.getElementsByClassName("open-partnerfilter")[0];
var openproductfilter = document.getElementsByClassName("open-productfilter")[0];
var openvariation = document.getElementsByClassName("open-variation")[0];
var openccv = document.getElementsByClassName("open-ccv")[0];
var openurl = document.getElementsByClassName("open-url")[0];
var opendesc = document.getElementsByClassName("open-desc")[0];
var opentutorial = document.getElementsByClassName("open-tutorial")[0];
var openconfirm = document.getElementsByClassName("open-confirm")[0];
var openconfirm1 = document.getElementsByClassName("open-confirm")[1];
var openconfirm2 = document.getElementsByClassName("open-confirm")[2];
var openconfirm3 = document.getElementsByClassName("open-confirm")[3];



var closeforgot = document.getElementsByClassName("close-forgot")[0];
var closeforgot2 = document.getElementsByClassName("close-forgot2")[0];
var closesignup = document.getElementsByClassName("close-signup")[0];
var closelogin = document.getElementsByClassName("close-login")[0];
var closefilter = document.getElementsByClassName("close-filter")[0];
var closesocial = document.getElementsByClassName("close-social")[0];
var closereport = document.getElementsByClassName("close-report")[0];
var closereview = document.getElementsByClassName("close-review")[0];
var closepartnerfilter = document.getElementsByClassName("close-partnerfilter")[0];
var closeproductfilter = document.getElementsByClassName("close-productfilter")[0];
var closevariation = document.getElementsByClassName("close-variation")[0];
var closeccv = document.getElementsByClassName("close-ccv")[0];
var closeurl = document.getElementsByClassName("close-url")[0];
var closedesc = document.getElementsByClassName("close-desc")[0];
var closetutorial = document.getElementsByClassName("close-tutorial")[0];
var closetutorial1 = document.getElementsByClassName("close-tutorial")[1];
var closeconfirm = document.getElementsByClassName("close-confirm")[0];
var closeconfirm2 = document.getElementsByClassName("close-confirm")[1];

if(openforgot){
openforgot.onclick = function() {
  forgotmodal.style.display = "block";
}
}
if(opensignup){
opensignup.onclick = function() {
  signupmodal.style.display = "block";
}
}
if(opensignup1){
opensignup1.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup2){
opensignup2.onclick = function() {
  signupmodal.style.display = "block";
  loginmodal.style.display = "none";
}
}
if(opensignup3){
opensignup3.onclick = function() {
  signupmodal.style.display = "block";
  forgot2modal.style.display = "none";
}
}
if(openlogin){
openlogin.onclick = function() {
  loginmodal.style.display = "block";
}
}
if(openlogin1){
openlogin1.onclick = function() {
  loginmodal.style.display = "block";

}
}
if(openlogin2){
openlogin2.onclick = function() {
  loginmodal.style.display = "block";
  signupmodal.style.display = "none";

}
}
if(openlogin3){
openlogin3.onclick = function() {
  loginmodal.style.display = "block";
	signupmodal.style.display = "none";
}
}
if(openlogin4){
openlogin4.onclick = function() {
  loginmodal.style.display = "block";
	forgot2modal.style.display = "none";
}
}
if(openforgot2){
openforgot2.onclick = function() {
  loginmodal.style.display = "none";
	forgot2modal.style.display = "block";
}
}
if(openfilter){
openfilter.onclick = function() {
  filtermodal.style.display = "none";
	filtermodal.style.display = "block";
}
}
if(opensocial){
opensocial.onclick = function() {
  socialmodal.style.display = "none";
	socialmodal.style.display = "block";
}
}
if(openreport){
openreport.onclick = function() {
  reportmodal.style.display = "block";
}
}
if(openreport1){
openreport1.onclick = function() {
  reportmodal.style.display = "block";
}
}
if(openreport2){
openreport2.onclick = function() {
  reportmodal.style.display = "block";
}
}
if(openreview){
openreview.onclick = function() {
  reviewmodal.style.display = "block";
}
}
if(openreview1){
openreview1.onclick = function() {
  reviewmodal.style.display = "block";
}
}
if(openpartnerfilter){
openpartnerfilter.onclick = function() {
  partnerfiltermodal.style.display = "block";
}
}
if(openproductfilter){
openproductfilter.onclick = function() {
  productfiltermodal.style.display = "block";
}
}
if(openvariation){
openvariation.onclick = function() {
  variationmodal.style.display = "block";
}
}
if(openccv){
openccv.onclick = function() {
  ccvmodal.style.display = "block";
}
}
if(openurl){
openurl.onclick = function() {
  urlmodal.style.display = "block";
}
}
if(opendesc){
opendesc.onclick = function() {
  descmodal.style.display = "block";
}
}
if(opentutorial){
opentutorial.onclick = function() {
  tutorialmodal.style.display = "block";
}
}
if(openconfirm){
openconfirm.onclick = function() {
  confirmmodal.style.display = "block";
}
}
if(openconfirm1){
openconfirm1.onclick = function() {
  confirmmodal.style.display = "block";
}
}
if(openconfirm2){
openconfirm2.onclick = function() {
  confirmmodal.style.display = "block";
}
}
if(openconfirm3){
openconfirm3.onclick = function() {
  confirmmodal.style.display = "block";
}
}


if(closeforgot){
closeforgot.onclick = function() {
  forgotmodal.style.display = "none";
}
}
if(closeforgot2){
closeforgot2.onclick = function() {
  forgot2modal.style.display = "none";
}
}
if(closesignup){
closesignup.onclick = function() {
  signupmodal.style.display = "none";
}
}
if(closelogin){
closelogin.onclick = function() {
  loginmodal.style.display = "none";
}
}
if(closefilter){
closefilter.onclick = function() {
  filtermodal.style.display = "none";
}
}
if(closesocial){
closesocial.onclick = function() {
  socialmodal.style.display = "none";
}
}
if(closereport){
closereport.onclick = function() {
  reportmodal.style.display = "none";
}
}
if(closereview){
closereview.onclick = function() {
  reviewmodal.style.display = "none";
}
}
if(closepartnerfilter){
closepartnerfilter.onclick = function() {
  partnerfiltermodal.style.display = "none";
}
}
if(closeproductfilter){
closeproductfilter.onclick = function() {
  productfiltermodal.style.display = "none";
}
}
if(closevariation){
closevariation.onclick = function() {
  variationmodal.style.display = "none";
}
}
if(closeccv){
closeccv.onclick = function() {
  ccvmodal.style.display = "none";
}
}
if(closeurl){
closeurl.onclick = function() {
  urlmodal.style.display = "none";
}
}
if(closedesc){
closedesc.onclick = function() {
  descmodal.style.display = "none";
}
}
if(closetutorial){
closetutorial.onclick = function() {
  tutorialmodal.style.display = "none";
}
}
if(closetutorial1){
closetutorial1.onclick = function() {
  tutorialmodal.style.display = "none";
}
}
if(closeconfirm){
closeconfirm.onclick = function() {
  confirmmodal.style.display = "none";
}
}
if(closeconfirm2){
closeconfirm2.onclick = function() {
  confirmmodal.style.display = "none";
}
}

window.onclick = function(event) {
  if (event.target == forgotmodal) {
    forgotmodal.style.display = "none";
  }
  if (event.target == signupmodal) {
    signupmodal.style.display = "none";
  }  
  if (event.target == loginmodal) {
    loginmodal.style.display = "none";
  }  
  if (event.target == forgot2modal) {
    forgot2modal.style.display = "none";
  }  
  if (event.target == filtermodal) {
    filtermodal.style.display = "none";
  } 
  if (event.target == socialmodal) {
    socialmodal.style.display = "none";
  } 
  if (event.target == reportmodal) {
    reportmodal.style.display = "none";
  } 
  if (event.target == reviewmodal) {
    reviewmodal.style.display = "none";
  } 
  if (event.target == partnerfiltermodal) {
    partnerfiltermodal.style.display = "none";
  }   
  if (event.target == productfiltermodal) {
    productfiltermodal.style.display = "none";
  } 
  if (event.target == variationmodal) {
    variationmodal.style.display = "none";
  }  
  if (event.target == ccvmodal) {
    ccvmodal.style.display = "none";
  }    
  if (event.target == urlmodal) {
    urlmodal.style.display = "none";
  }     
  if (event.target == descmodal) {
    descmodal.style.display = "none";
  }  
  if (event.target == tutorialmodal) {
    tutorialmodal.style.display = "none";
  }  
  if (event.target == confirmmodal) {
    confirmmodal.style.display = "none";
  }             
}
</script>
<script type="text/javascript" src="js/modernizr.custom.js"></script>
<script type="text/javascript" src="js/jquery.dlmenu.js"></script>
		<script>
			$(function() {
				$( '#dl-menu' ).dlmenu({
					animationClasses : { classin : 'dl-animate-in-2', classout : 'dl-animate-out-2' }
				});
			});
		</script>
<script>
function goBack() {
  window.history.back();
}
</script>

<script src="js/jssor.slider.min.js" type="text/javascript"></script>
<script type="text/javascript">

        jssor_1_slider_init = function() {

            var jssor_1_options = {
              $AutoPlay: 1,
              $SlideDuration: 800,
              $SlideEasing: $Jease$.$OutQuint,
              $ArrowNavigatorOptions: {
                $Class: $JssorArrowNavigator$
              },
              $BulletNavigatorOptions: {
                $Class: $JssorBulletNavigator$
              }
            };

            var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);

            /*#region responsive code begin*/

            var MAX_WIDTH = 3000;

            function ScaleSlider() {
                var containerElement = jssor_1_slider.$Elmt.parentNode;
                var containerWidth = containerElement.clientWidth;

                if (containerWidth) {

                    var expectedWidth = Math.min(MAX_WIDTH || containerWidth, containerWidth);

                    jssor_1_slider.$ScaleWidth(expectedWidth);
                }
                else {
                    window.setTimeout(ScaleSlider, 30);
                }
            }

            ScaleSlider();

            $Jssor$.$AddEvent(window, "load", ScaleSlider);
            $Jssor$.$AddEvent(window, "resize", ScaleSlider);
            $Jssor$.$AddEvent(window, "orientationchange", ScaleSlider);
            /*#endregion responsive code end*/
        };
    </script>
    
    
    <script type="text/javascript">jssor_1_slider_init();</script>
    <!-- #endregion Jssor Slider End -->

  <script src="./slick/slick.js" type="text/javascript" charset="utf-8"></script>
  <script type="text/javascript">
    $(document).on('ready', function() {
      $(".variable").slick({
        dots: false,
        infinite: true,
        variableWidth: true
      });
	  $(".regular").slick({
        dots: true,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1
      });
    });
  </script>
<script>
function openTab(evt, tabName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(tabName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
<script>
$(function(){$(".numbers-row1").append('<div class="dec button2 hover1 button"><img src="img/minus.png" class="hover1a add-minus" alt="Minus" title="Minus"><img src="img/minus2.png" class="hover1b add-minus" alt="Minus" title="Minus"></div><label for="name" class="quantity-label"></label><input type="text" name="variation1-input" id="variation1-input" value="0" class="quantity-input"><div class="inc button2 hover1 button"><span class="display-none">+</span><img src="img/add.png" class="hover1a add-minus" alt="Add" title="Add"><img src="img/add2.png" class="hover1b add-minus" alt="Add" title="Add" onclick="myFunction1()"></div>');$(".button").on("click",function(){var $button=$(this);var oldValue=$button.parent().find("input").val();if($button.text()=="+"){var newVal=parseFloat(oldValue)+1;}else{if(oldValue>0){var newVal=parseFloat(oldValue)-1;}else{newVal=0;}}
$button.parent().find("input").val(newVal);});});

</script>
<script>
$(function(){$(".numbers-row2").append('<div class="dec button2 hover1 button"><img src="img/minus.png" class="hover1a add-minus" alt="Minus" title="Minus"><img src="img/minus2.png" class="hover1b add-minus" alt="Minus" title="Minus"></div><label for="name" class="quantity-label"></label><input type="text" name="variation2-input" id="variation2-input" value="0" class="quantity-input"><div class="inc button2 hover1 button"><span class="display-none">+</span><img src="img/add.png" class="hover1a add-minus" alt="Add" title="Add"><img src="img/add2.png" class="hover1b add-minus" alt="Add" title="Add" onclick="myFunction2()"></div>');$(".button").on("click",function(){var $button=$(this);var oldValue=$button.parent().find("input").val();if($button.text()=="+"){var newVal=parseFloat(oldValue)+1;}else{if(oldValue>0){var newVal=parseFloat(oldValue)-1;}else{newVal=0;}}
$button.parent().find("input").val(newVal);});});

</script>
<script>
$(function(){$(".numbers-row3").append('<div class="dec button2 hover1 button"><img src="img/minus.png" class="hover1a add-minus" alt="Minus" title="Minus"><img src="img/minus2.png" class="hover1b add-minus" alt="Minus" title="Minus"></div><label for="name" class="quantity-label"></label><input type="text" name="variation3-input" id="variation3-input" value="0" class="quantity-input"><div class="inc button2 hover1 button"><span class="display-none">+</span><img src="img/add.png" class="hover1a add-minus" alt="Add" title="Add"><img src="img/add2.png" class="hover1b add-minus" alt="Add" title="Add" onclick="myFunction3()"></div>');$(".button").on("click",function(){var $button=$(this);var oldValue=$button.parent().find("input").val();if($button.text()=="+"){var newVal=parseFloat(oldValue)+1;}else{if(oldValue>0){var newVal=parseFloat(oldValue)-1;}else{newVal=0;}}
$button.parent().find("input").val(newVal);});});

</script>
<script>
$(function(){$(".numbers-row4").append('<div class="dec button2 hover1 button"><img src="img/minus.png" class="hover1a add-minus" alt="Minus" title="Minus"><img src="img/minus2.png" class="hover1b add-minus" alt="Minus" title="Minus"></div><label for="name" class="quantity-label"></label><input type="text" name="variation4-input" id="variation4-input" value="0" class="quantity-input"><div class="inc button2 hover1 button"><span class="display-none">+</span><img src="img/add.png" class="hover1a add-minus" alt="Add" title="Add"><img src="img/add2.png" class="hover1b add-minus" alt="Add" title="Add" onclick="myFunction4()"></div>');$(".button").on("click",function(){var $button=$(this);var oldValue=$button.parent().find("input").val();if($button.text()=="+"){var newVal=parseFloat(oldValue)+1;}else{if(oldValue>0){var newVal=parseFloat(oldValue)-1;}else{newVal=0;}}
$button.parent().find("input").val(newVal);});});

</script>
<script>
$(function(){$(".numbers-row5").append('<div class="dec button2 hover1 button"><img src="img/minus.png" class="hover1a add-minus" alt="Minus" title="Minus"><img src="img/minus2.png" class="hover1b add-minus" alt="Minus" title="Minus"></div><label for="name" class="quantity-label"></label><input type="text" name="variation5-input" id="variation5-input" value="0" class="quantity-input"><div class="inc button2 hover1 button"><span class="display-none">+</span><img src="img/add.png" class="hover1a add-minus" alt="Add" title="Add"><img src="img/add2.png" class="hover1b add-minus" alt="Add" title="Add" onclick="myFunction5()"></div>');$(".button").on("click",function(){var $button=$(this);var oldValue=$button.parent().find("input").val();if($button.text()=="+"){var newVal=parseFloat(oldValue)+1;}else{if(oldValue>0){var newVal=parseFloat(oldValue)-1;}else{newVal=0;}}
$button.parent().find("input").val(newVal);});});

</script>
<script>
// File Upload
// 
function ekUpload(){
  function Init() {

    console.log("Upload Initialised");

    var fileSelect    = document.getElementById('file-upload'),
        fileDrag      = document.getElementById('file-drag'),
        submitButton  = document.getElementById('submit-button');

    fileSelect.addEventListener('change', fileSelectHandler, false);

    // Is XHR2 available?
    var xhr = new XMLHttpRequest();
    if (xhr.upload) {
      // File Drop
      fileDrag.addEventListener('dragover', fileDragHover, false);
      fileDrag.addEventListener('dragleave', fileDragHover, false);
      fileDrag.addEventListener('drop', fileSelectHandler, false);
    }
  }

  function fileDragHover(e) {
    var fileDrag = document.getElementById('file-drag');

    e.stopPropagation();
    e.preventDefault();

    fileDrag.className = (e.type === 'dragover' ? 'hover' : 'modal-body file-upload');
  }

  function fileSelectHandler(e) {
    // Fetch FileList object
    var files = e.target.files || e.dataTransfer.files;

    // Cancel event and hover styling
    fileDragHover(e);

    // Process all File objects
    for (var i = 0, f; f = files[i]; i++) {
      parseFile(f);
      uploadFile(f);
    }
  }

  // Output
  function output(msg) {
    // Response
    var m = document.getElementById('messages');
    m.innerHTML = msg;
  }

  function parseFile(file) {

    console.log(file.name);
    output(
      '<strong>' + encodeURI(file.name) + '</strong>'
    );
    
    // var fileType = file.type;
    // console.log(fileType);
    var imageName = file.name;

    var isGood = (/\.(?=gif|jpg|png|jpeg)/gi).test(imageName);
    if (isGood) {
      document.getElementById('start').classList.add("hidden");
      document.getElementById('response').classList.remove("hidden");
      document.getElementById('notimage').classList.add("hidden");
      // Thumbnail Preview
      document.getElementById('file-image').classList.remove("hidden");
      document.getElementById('file-image').src = URL.createObjectURL(file);
    }
    else {
      document.getElementById('file-image').classList.add("hidden");
      document.getElementById('notimage').classList.remove("hidden");
      document.getElementById('start').classList.remove("hidden");
      document.getElementById('response').classList.add("hidden");
      document.getElementById("file-upload-form").reset();
    }
  }

  function setProgressMaxValue(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.max = e.total;
    }
  }

  function updateFileProgress(e) {
    var pBar = document.getElementById('file-progress');

    if (e.lengthComputable) {
      pBar.value = e.loaded;
    }
  }

  function uploadFile(file) {

    var xhr = new XMLHttpRequest(),
      fileInput = document.getElementById('class-roster-file'),
      pBar = document.getElementById('file-progress'),
      fileSizeLimit = 1024; // In MB
    if (xhr.upload) {
      // Check if file is less than x MB
      if (file.size <= fileSizeLimit * 1024 * 1024) {
        // Progress bar
        pBar.style.display = 'inline';
        xhr.upload.addEventListener('loadstart', setProgressMaxValue, false);
        xhr.upload.addEventListener('progress', updateFileProgress, false);

        // File received / failed
        xhr.onreadystatechange = function(e) {
          if (xhr.readyState == 4) {
            // Everything is good!

            // progress.className = (xhr.status == 200 ? "success" : "failure");
            // document.location.reload(true);
          }
        };

        // Start upload
        xhr.open('POST', document.getElementById('file-upload-form').action, true);
        xhr.setRequestHeader('X-File-Name', file.name);
        xhr.setRequestHeader('X-File-Size', file.size);
        xhr.setRequestHeader('Content-Type', 'multipart/form-data');
        xhr.send(file);
      } else {
        output('Please upload a smaller file (< ' + fileSizeLimit + ' MB).');
      }
    }
  }

  // Check for the various File API support.
  if (window.File && window.FileList && window.FileReader) {
    Init();
  } else {
    document.getElementById('file-drag').style.display = 'none';
  }
}
ekUpload();
</script>


<script type="text/javascript" src="js/main.js?version=1.0.1"></script>

<script>
function showVariation() {
  var x = document.getElementById("variationDiv");
  if (x.style.display === "none") {
    x.style.display = "block";
  } else {
    x.style.display = "none";
  }
}
</script>

		