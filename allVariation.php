<?php
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Variation.php';

require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$productUid = $_POST['user_id'];

$conn = connDB();

$variationDetails = getVariation($conn," WHERE status = ? ",array("status"),array("available"),"s");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="All Product Variation | Mypetslibrary" />
<title>All Product Variation | Mypetslibrary</title>
<meta property="og:description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="description" content="Mypetslibrary serves as Asia’s 1st established professional platform featuring pets that connects top pet sellers and buyers across nationwide. Buyers who are ready to have a pet may look into Mypetslibrary to search for their preferred breed or getting advice from us." />
<meta name="keywords" content="Mypetslibrary, my pets library, my pet library,pet, online pet store, pet seller, cat,kitten, dog,puppy, reptile, dog food, pet food, pet product, pet grooming, 宠物,线上宠物店,小狗,猫咪,蜥蜴, etc">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance" id="myTable">
	<div class="width100">
        <div class="left-h1-div">
            <h1 class="green-text h1-title">All Variation</h1>
            <div class="green-border"></div>
        </div>
        <div class="mid-search-div">
        <form>
            <input class="line-input clean" type="text" id="myInput" onkeyup="myFunction()" placeholder="Search">
                <button class="search-btn hover1 clean">
                    <img src="img/search.png" class="visible-img hover1a" alt="Search" title="Search">
                    <img src="img/search2.png" class="visible-img hover1b" alt="Search" title="Search">
                </button>
            </form>
        </div>
        <div class="right-add-div">
        	<a href="<?php echo "./addVariation.php?id=".$productUid?>"><div class="green-button white-text puppy-button">Add Variation</div></a>
        </div>      
    </div>


    <div class="clear"></div>
	<div class="width100 scroll-div border-separation">
    	<table class="green-table width100">
        	<thead>
            	<tr>
                	<th class="first-column">No.</th>
                    <th>Variation Name</th>
                    <th>Variation Brand</th>
                    <th>Variation Category</th>
                    <th>For</th>
                    <th>Stock</th>
                    <th>Added On</th>
                    <th>Status</th>
                    <th>Details</th>
                    <th>Delete</th>                    
                </tr>
            </thead>
            <tbody>
            <?php
            $count=0;
            if($variationDetails)
            {
                if(isset($_POST['user_id']))
                {
                    for($cnt = 0;$cnt < count($variationDetails) ;$cnt++)
                    {
                        if($variationDetails[$cnt]->getProductId()== $productUid)
                        {
                        ?>
                        
                            <tr class="link-to-details">
                                <td><?php echo ($count+1)?>.</td>
                                <td><?php echo $variationDetails[$cnt]->getVariation();?></td>
                                <td><?php echo $variationDetails[$cnt]->getBrand();?></td>
                                <td><?php echo $variationDetails[$cnt]->getCategory();?></td>
                                <td><?php echo $variationDetails[$cnt]->getAnimalType();?></td>
                                <td><?php echo $variationDetails[$cnt]->getVariationStock();?></td>
                                <td><?php echo $variationDetails[$cnt]->getDateCreated();?></td>
                                <td><?php echo $variationDetails[$cnt]->getStatus();?></td>
                                <td>
                                    <form action="editVariation.php" method="POST" class="hover1">
                                        <button class="clean hover1 img-btn transparent-button pointer" type="submit" name="variation_id" value="<?php echo $variationDetails[$cnt]->getId();?>">
                                            <img src="img/edit1a.png" class="edit-icon1 hover1a" alt="Edit" title="Edit">
                                            <img src="img/edit3a.png" class="edit-icon1 hover1b" alt="Edit" title="Edit">
                                        </button>
                                    </form>
                                </td>
                                <td>
                                        <a class="hover1 open-confirm pointer">
                                            <img src="img/delete1a.png" class="edit-icon1 hover1a" alt="Delete" title="Delete">
                                            <img src="img/delete3a.png" class="edit-icon1 hover1b" alt="Delete" title="Delete">
                                        </a>                    
                                </td>
                            </tr>
                            <?php
                            $count++; 
                            }  
                        }
                    }
                }
                ?>                                 
            </tbody>
        </table>
    </div>
    <div class="clear"></div>
    <div class="width100 bottom-spacing"></div>

</div>
<div class="clear"></div>

<?php include 'js.php'; ?>

<script>
function myFunction() {
  var input, filter, table, tr, td, i, txtValue;
  input = document.getElementById("myInput");
  filter = input.value.toUpperCase();
  table = document.getElementById("myTable");
  tr = table.getElementsByTagName("tr");
  for (i = 0; i < tr.length; i++) {
    td = tr[i].getElementsByTagName("td")[1];
    if (td) {
      txtValue = td.textContent || td.innerText;
      if (txtValue.toUpperCase().indexOf(filter) > -1) {
        tr[i].style.display = "";
      } else {
        tr[i].style.display = "none";
      }
    }       
  }
}
</script>

</body>
</html>