<?php
require_once dirname(__FILE__) . '/adminAccess.php';
require_once dirname(__FILE__) . '/sessionLoginChecker.php';
require_once dirname(__FILE__) . '/1dbCon/dbCon.php';

require_once dirname(__FILE__) . '/classes/Article.php';
require_once dirname(__FILE__) . '/classes/Seller.php';
require_once dirname(__FILE__) . '/classes/User.php';
require_once dirname(__FILE__) . '/classes/Kitten.php';
require_once dirname(__FILE__) . '/classes/Orders.php';
require_once dirname(__FILE__) . '/classes/Pets.php';
require_once dirname(__FILE__) . '/classes/Puppy.php';
require_once dirname(__FILE__) . '/classes/Reptile.php';
require_once dirname(__FILE__) . '/classes/Reviews.php';

require_once dirname(__FILE__) . '/utilities/allNoticeModals.php';
require_once dirname(__FILE__) . '/utilities/databaseFunction.php';
require_once dirname(__FILE__) . '/utilities/generalFunction.php';

$uid = $_SESSION['uid'];

$conn = connDB();

$userDetails = getUser($conn, "WHERE uid =?",array("uid"),array($uid),"s");
$userData = $userDetails[0];

$userAmount = getUser($conn," WHERE user_type = 1 ");
$sellerAmount = getSeller($conn," WHERE account_status = 'Active' ");
$kittenAmount = getKitten($conn);
$puppyAmount = getPuppy($conn);
$reptileAmount = getReptile($conn);
$petAmount = getPetsDetails($conn," WHERE status = 'Available' ");
$pendingPet = getPetsDetails($conn," WHERE status = 'Pending' ");
$articles = getArticles($conn, " WHERE display = 'Pending' ");
$reviews = getReviews($conn, " WHERE display = 'Pending' ");
$shippingRequest = getOrders($conn, " WHERE payment_status = 'ACCEPTED' AND shipping_status = 'PENDING' ");

$conn->close();

function promptError($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}

function promptSuccess($msg){
    echo '
        <script>
            alert("'.$msg.'");
        </script>
    ';
}
?>

<!doctype html>
<html>
<head>
<?php include 'meta.php'; ?>
<!--<meta property="og:url" content="https://thousandmedia.asia/" />-->
<meta property="og:title" content="Admin Dashboard" />
<title>Admin Dashboard</title>
<meta property="og:description" content="" />
<meta name="description" content="" />
<meta name="keywords" content="">
<!--<link rel="canonical" href="https://thousandmedia.asia/" />-->
<?php include 'css.php'; ?>
</head>

<body class="body">
<?php include 'header.php'; ?>
<div class="width100 same-padding menu-distance admin-min-height-with-distance eight-div-box">
	<h1 class="green-text h1-title">Dashboard</h1>
	<div class="green-border"></div>
    <div class="clear"></div>

        <div class="width100 border-separation">
        
        <a  href="pendingReview.php"class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div six-box last-three-div">
            <img src="img/partner.png" alt="Seller" title="Seller" class="four-div-img">
                <p class="four-div-p">Seller</p>

                <?php
                if($sellerAmount)
                {   
                    $totalSeller = count($sellerAmount);
                }
                else
                {   $totalSeller = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalSeller;?></b></p>
                <a href="addSeller.php"><p class="dashboard-p"><b>Add</b></p></a>
                <a href="seller.php"><p class="dashboard-p dashboard-p2"><b>View</b></p></a>
            </div> 
        </a> 


        <a  href="pendingReview.php"class="opacity-hover">
            <div class="white-dropshadow-box four-div-box last-three-div">
                <img src="img/review-pet.png" alt="Pending Reviews" title="Pending Reviews" class="four-div-img">
                <p class="four-div-p last-three-p last-two-p">Pending Reviews</p>
                <?php
                if($reviews)
                {   
                    $totalPendingReviews = count($reviews);
                }
                else
                {   $totalPendingReviews = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalPendingReviews;?></b></p>
            </div> 
        </a>  

        <a href="seller.php" class="opacity-hover">
            <div class="white-dropshadow-box four-div-box second-four-div-box left-four-div six-box last-three-div">
                 <img src="img/pet-sellers.png" alt="Total Users" title="Total Users" class="four-div-img">
                <p class="four-div-p last-three-p last-two-p">Total Users</p>

                <?php
                if($userAmount)
                {   
                    $totalUser = count($userAmount);
                }
                else
                {   $totalUser = 0;   }
                ?>
                <p class="four-div-amount-p"><b><?php echo $totalUser;?></b></p>
            </div> 
        </a>
            
        </div>

        <div class="clear"></div>
</div>
<?php include 'js.php'; ?>

<?php
if(isset($_GET['type']))
{
    $messageType = null;

    if($_SESSION['messageType'] == 1)
    {
        if($_GET['type'] == 1)
        {
            $messageType = "Update Password Successfully"; 
        }
        else if($_GET['type'] == 2)
        {
            // $messageType = "Fail to update password !! ";
        }
        echo '
        <script>
            putNoticeJavascript("Notice !! ","'.$messageType.'");  
        </script>
        ';   
        $_SESSION['messageType'] = 0;
    }
}
?>

</body>
</html>